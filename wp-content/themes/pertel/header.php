<!DOCTYPE html>

<html <?php language_attributes(); ?> class="no-js">

<head>

	<meta charset="<?php bloginfo( 'charset' ); ?>">

	<meta name="viewport" content="width=device-width">


	<title><?php wp_title( '|', true, 'right' ); ?></title>


	<link rel="profile" href="http://gmpg.org/xfn/11">


	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">


	<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/bootstrap.min.css">


    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/js/vendor/slidebars.min.css">

    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/main.css?v=70">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

    <link href='https://fonts.googleapis.com/css?family=Lato:300,900,700,400' rel='stylesheet' type='text/css'>

    <link rel="shortcut icon" type="image/x-icon" href="http://www.perteldeperu.com/wp-content/uploads/2016/08/favicon.ico"/>

    <script src="<?php echo get_template_directory_uri() ?>/js/vendor/modernizr-2.8.3.min.js"></script>

    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

	<?php wp_head(); ?>

    <div id="fb-root"></div>

    <script>(function(d, s, id) {

    var js, fjs = d.getElementsByTagName(s)[0];

    if (d.getElementById(id)) return;

    js = d.createElement(s); js.id = id;

    js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.7&appId=238310832987958";

    fjs.parentNode.insertBefore(js, fjs);


    }(document, 'script', 'facebook-jssdk'));</script>

    <style>

        .productos aside nav ul{ padding: 0 0 0 1.5em;}

        .recapcha > div, #recapcha-contato > div{width:100%;}

        #menu-menu-categoria-productos{ padding:0;}

        .productos aside nav ul li.current-cat > a{background-color: #1a1c27;color: #fff;}

        [off-canvas*=left]{z-index: 999;}

    </style>

</head>


<body <?php body_class(); ?>>

	<div off-canvas="id-1 left reveal">


            <nav class="menu sidebar">


                <h3>MENU <span><i class="fa fa-times" aria-hidden="true"></i></span></h3>


                <?php 

                    wp_nav_menu( array(


                        'menu' => 'Menu Principal'

                    ) );

                 ?>
            </nav>
        </div>
        <header>
                <div class="container">
                    <div class="row"> 
                        <div class="col-md-12">
                            <a href="<?php echo site_url(); ?>/" class="logo" item>
                                <img itemprop="image" src="<?php echo get_template_directory_uri() ?>/img/logo-pertel.png" alt="Pertel">
                            </a>
                            <a href="javascript:;" class="menu-dropdown">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </a>
                            <nav class="menu">
                                <?php 

                                    wp_nav_menu( array(

                                        'menu' => 'Menu Principal'

                                    ) );

                                 ?>

                            </nav>

                        </div>

                    </div>

                </div>
        </header>
        <div canvas="container" >