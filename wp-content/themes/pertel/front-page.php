<?php get_header(); ?>

<section class="content-page home">
        
    <section class="slider">
        <div class="box">
            <ul>
            	<?php $slider = new WP_Query( array(  'post_type' => 'slider' ) ); ?> 
            	<?php while ( $slider->have_posts() ) : $slider->the_post(); ?>               
                <li>
                    <a href="<?php the_field('link') ?>">
                    	<?php if ( wp_is_mobile() ) { ?>
                    		<img src="<?php the_field('slide_responsive') ?>" alt="<?php the_title() ?>" />
                    	<?php }else{ ?>
                        	<img src="<?php the_field('slide') ?>" alt="<?php the_title() ?>" />
                        <?php } ?>
                    </a>
                </li>
                <?php endwhile; ?>
            </ul>
        </div>
    </section>
    <section class="featured">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="heading"> <strong> Productos </strong> destacados </h2>
                </div>
                <div class="clear"></div>
	
				<?php $featured_products = new WP_Query( array( 'meta_key' => '_is_ns_featured_post', 'meta_value' => 'yes', 'post_type' => 'productos','posts_per_page' => '4' ) ); ?>                
	
				<?php while ( $featured_products->have_posts() ) : $featured_products->the_post(); ?>
	                <div class="col-md-3 col-sm-6 box">
	                    <article>
	                        <figure>
	                            <a href="<?php the_permalink(); ?>">
	                                <img src="<?php the_post_thumbnail_url( 'full' ); ?>" alt="the_title()" />
	                            </a>
	                        </figure>
	                        <h2>
	                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?> <br>
	                            <?php
	                             	$terms = get_the_terms( get_the_id(), 'categoria' );
	                             	$term = array_pop($terms);
	                            ?>
	                            <strong><?php echo $term->name ?></strong></a>
	                        </h2>
	                        
	                    </article>
	                </div>
                <?php endwhile; ?>
                
            </div>
        </div>                                  
    </section>
    <section class="services">
        <div class="container">
            <div class="row">
                <div class="col-md-12 heading">
                <?php $cabecera = new WP_Query( array(  'post_type' => 'cabeceras', 'name' => 'servicios' ) ); ?> 
            	<?php while ( $cabecera->have_posts() ) : $cabecera->the_post(); ?>        
                    <?php the_content(); ?>
                <?php endwhile; ?>
                </div>
            </div>

            <div class="row">
            	<?php 

            		$tipos = get_terms( 'tipo', array( 'hide_empty' => 0 ) );
            		$i=0;
					 if ( ! empty( $tipos ) && ! is_wp_error( $tipos ) ){
					    
					    foreach ( $tipos as $term ) { ?>
					    	
					    	<?php if ( $i % 2 == 0): ?>

					    		<?php $servicios = new WP_Query( array(  'post_type' => 'servicios', 'tipo'=>$term->slug

					    		 ) ); ?> 
            					       
                   
					    		<section class="tipo">
				                    <figure class="col-md-6 pic">
				                        <img src="<?php the_field('imagen', $term); ?>" alt="<?php echo $term->name; ?>">
				                    </figure>
				                    <article class="col-md-6">
				                        <div class="box-tipo">
				                        	
				                        	<h2><?php echo $term->name; ?></h2>
					                        <a href="<?php echo get_term_link($term); ?>#<?php the_slug(); ?>" class="mobile">
					                            <img src="<?php the_field('imagen', $term); ?>" alt="<?php echo $term->name; ?>">
					                        </a>
					                        <div class="row">
					                        	<?php $j=1; ?>
					                        	<?php while ( $servicios->have_posts() ) : $servicios->the_post(); ?> 
					                            <div class="col-md-6 col-sm-6 col-xs-12 item-serv">
					                                <div class="title"><span class="icono" style="background-image: url('<?php the_field('icono'); ?>');">&nbsp;</span>
					                                <h3><?php the_title(); ?></h3></div>
					                                <div class="clear"></div>
					                                <div class="excerpt">
					                                    <?php the_excerpt(); ?>
					                                </div>
					                                <a href="<?php echo get_term_link($term); ?>#<?php the_slug(); ?>" class="btn-vermas">Conocer más &#8594;</a>  
					                            </div>
					                            <?php if ($j % 2 == 0): ?>
					                            	<div class="clear"></div>
					                            <?php endif ?>
					                            <?php $j++; ?>
				                            	<?php endwhile; ?>
					                        </div>

				                        </div>

				                    </article>
				                </section>
				                
					    	<?php endif ?>
							
							<?php if ( $i % 2 != 0): ?>
								<?php $servicios = new WP_Query( array(  'post_type' => 'servicios',
					    				'tax_query' => array(
											array(
												'taxonomy' => 'tipo',
												'field'    => 'slug',
												'terms'    => $term->slug,
											),
										),
					    		 ) ); ?> 
            					        
                   
					    		<section class="tipo">
				                    
				                    <article class="col-md-6">
				                    	<div class="box-tipo">
					                        <h2><?php echo $term->name; ?></h2>
					                        <a href="<?php echo get_term_link($term); ?>#<?php the_slug(); ?>" class="mobile">
					                            <img src="<?php the_field('imagen', $term); ?>" alt="<?php echo $term->name; ?>">
					                        </a>
					                        
					                        <div class="row">
					                        	<?php $j=1; ?>
					                        	<?php while ( $servicios->have_posts() ) : $servicios->the_post(); ?>
					                            <div class="col-md-6 col-sm-6 col-xs-12 item-serv">
					                                <div class="title"><span class="icono" style="background-image: url('<?php the_field('icono'); ?>');">&nbsp;</span>
					                                <h3><?php the_title(); ?></h3></div>
					                                <div class="clear"></div>
					                                <div class="excerpt">
					                                    <?php the_excerpt(); ?>
					                                </div>
					                                <a href="<?php echo get_term_link($term); ?>#<?php the_slug(); ?>" class="btn-vermas">Conocer más &#8594;</a>  
					                            </div>
					                            <?php if ($j % 2 == 0): ?>
					                            	<div class="clear"></div>
					                            <?php endif ?>
					                            <?php $j++; ?>
				                            	<?php endwhile; ?>
					                        </div>
					                    </div>
				                    </article>
				                    <figure class="col-md-6 pic">
				                        <img src="<?php the_field('imagen', $term); ?>" alt="<?php echo $term->name; ?>">
				                    </figure>
				                </section>
				                
							<?php endif ?>

				                <div class="clear"></div>

				               	
					        
					    <?php 
					    $i++;
					    } 
					    
					 }
            	 ?>
                
                <div class="clear"></div>
                
                
            </div>
        </div>                                  
    </section>

    <!--<section class="news">
        <div class="container">
            <div class="row">
                <div class="col-md-12 heading">
                    <h2 > <strong> Noticias </strong> recientes </h2>
                </div>
                <?php $recent_posts = wp_get_recent_posts(array( 'numberposts' => '2' ));
				foreach( $recent_posts as $recent ){ ?>

					<article class="col-md-6">
	                    
	                    <figure>
	                        <div class="icons">
	                            <a href="<?php echo get_permalink($recent["ID"]) ?>"><i class="fa fa-link" aria-hidden="true"></i></a>
	                        </div>
	                        <span class="date">20 <br> Abr</span>
	                        <?php $thumb = get_the_post_thumbnail($recent["ID"]); ?>

	                        <?php echo $thumb; ?>
	                    </figure>
	                    <div class="info">
	                        <h3><a href="<?php echo get_permalink($recent["ID"]) ?>"><?php echo $recent["post_title"]; ?></a></h3>
	                        <p class="excerpt">
	                            <a href="<?php echo get_permalink($recent["ID"]) ?>">
	                            	<?php echo $recent["post_excerpt"]; ?>
	                            </a>
	                        </p>
	                    </div>
	                </article>

				<?php } ?>
	                

            </div>
        </div>
    </section>-->
</section>

<?php get_footer(); ?>