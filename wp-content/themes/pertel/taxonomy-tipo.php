<?php get_header(); ?>
<section class="content-page servicios">
    <style type="text/css">
    .fancybox-title{
        display: none;
    }

    </style>
    <section class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php if ( function_exists('yoast_breadcrumb') ) {
                            yoast_breadcrumb('
                            <p id="breadcrumbs" class="pull-right">','</p>
                            ');
                        }
                    ?>
                    <h2> <span><img src="<?php echo get_template_directory_uri() ?>/img/icon-servicios.png"></span> Servicios</h2>
                </div>
            </div>
        </div>
    </section>
    <section class="detalle">
        <div class="container">
            <div class="row">
                <div class="col-md-12 heading">
                    <?php $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); ?>
                    <h2><?php echo $term->name; ?></h2>

                </div>
            </div>

            <div class="row">
            <?php while ( have_posts() ) : the_post(); ?>
                <section class="col-md-12 info" id="<?php the_slug(); ?>">
                    <img src="<?php the_field('icono'); ?>" class="right-icon" alt="<?php the_title(); ?>" style="background-color: #0d9b95;float: left;border-radius: 4px;    margin-right: 1em;">
                    <h2 style="margin-top: .35em;"> <?php the_title(); ?></h2>
                    <article>
                        <?php the_content(); ?>
                    </article>
                    <div class="clear"></div>
                    <a href="#solicitar" title="<?php the_title(); ?>" class="solicitar various">SOLICITAR SERVICIO</a>
                </section>
            <?php endwhile; ?>
            </div>
        </div>        
                                  
    </section>
</section>
<div class="hidden">
    <div id="solicitar">
        <?php echo do_shortcode( '[contact-form-7 id="196" title="Servicios"]' ); ?>
    </div>
</div>
<?php get_footer(); ?>