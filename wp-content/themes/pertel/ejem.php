<?php 
   /**
    * Template Name: Terminos y Condiciones
    *
    */
   ?>
<!DOCTYPE html>
<html lang="en-US">
   <head>
      <style type="text/css">
         .menu_icon_wrapper .fa-facebook {font-size:23px !important;}
         .menu_icon_wrapper .fa-twitter {font-size:23px !important;}
         .menu_icon_wrapper .fa-instagram {font-size:23px !important;}
         #nav-menu-item-348511 .second{display:none !important;}
      </style>
      <meta charset="UTF-8" />
      <link rel="profile" href="http://gmpg.org/xfn/11" />
      <link rel="pingback" href="http://ayni.com.pe/xmlrpc.php" />
      <link rel="shortcut icon" type="image/x-icon" href="http://wplind.com/wp-content/uploads/2016/02/favicon.png">
      <link rel="apple-touch-icon" href="http://wplind.com/wp-content/uploads/2016/02/favicon.png"/>
      <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
      <title>Terms - AYNI</title>
      <link rel="alternate" type="application/rss+xml" title="AYNI &raquo; Feed" href="http://ayni.com.pe/feed/" />
      <link rel="alternate" type="application/rss+xml" title="AYNI &raquo; Comments Feed" href="http://ayni.com.pe/comments/feed/" />
      <script type="text/javascript">
         window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/ayni.com.pe\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.5.3"}};
         !function(a,b,c){function d(a){var c,d,e,f=b.createElement("canvas"),g=f.getContext&&f.getContext("2d"),h=String.fromCharCode;if(!g||!g.fillText)return!1;switch(g.textBaseline="top",g.font="600 32px Arial",a){case"flag":return g.fillText(h(55356,56806,55356,56826),0,0),f.toDataURL().length>3e3;case"diversity":return g.fillText(h(55356,57221),0,0),c=g.getImageData(16,16,1,1).data,d=c[0]+","+c[1]+","+c[2]+","+c[3],g.fillText(h(55356,57221,55356,57343),0,0),c=g.getImageData(16,16,1,1).data,e=c[0]+","+c[1]+","+c[2]+","+c[3],d!==e;case"simple":return g.fillText(h(55357,56835),0,0),0!==g.getImageData(16,16,1,1).data[0];case"unicode8":return g.fillText(h(55356,57135),0,0),0!==g.getImageData(16,16,1,1).data[0]}return!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i;for(i=Array("simple","flag","unicode8","diversity"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
      </script>
      <style type="text/css">
         img.wp-smiley,
         img.emoji {
         display: inline !important;
         border: none !important;
         box-shadow: none !important;
         height: 1em !important;
         width: 1em !important;
         margin: 0 .07em !important;
         vertical-align: -0.1em !important;
         background: none !important;
         padding: 0 !important;
         }
      </style>
      <link rel='stylesheet' id='dashicons-css'  href='http://ayni.com.pe/wp-includes/css/dashicons.min.css?ver=4.5.3' type='text/css' media='all' />
      <link rel='stylesheet' id='wp-jquery-ui-dialog-css'  href='http://ayni.com.pe/wp-includes/css/jquery-ui-dialog.min.css?ver=4.5.3' type='text/css' media='all' />
      <link rel='stylesheet' id='open-sans-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&#038;subset=latin%2Clatin-ext&#038;ver=4.5.3' type='text/css' media='all' />
      <link rel='stylesheet' id='admin-bar-css'  href='http://ayni.com.pe/wp-includes/css/admin-bar.min.css?ver=4.5.3' type='text/css' media='all' />
      <link rel='stylesheet' id='contact-form-7-css'  href='http://ayni.com.pe/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=4.4.2' type='text/css' media='all' />
      <link rel='stylesheet' id='rs-plugin-settings-css'  href='http://ayni.com.pe/wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.1.5' type='text/css' media='all' />
      <style id='rs-plugin-settings-inline-css' type='text/css'>
         #rs-demo-id {}
      </style>
      <link rel='stylesheet' id='eltd_moose_default_style-css'  href='http://ayni.com.pe/wp-content/themes/moose/style.css?ver=4.5.3' type='text/css' media='all' />
      <link rel='stylesheet' id='eltd_moose_stylesheet-css'  href='http://ayni.com.pe/wp-content/themes/moose/css/stylesheet.min.css?ver=4.5.3' type='text/css' media='all' />
      <link rel='stylesheet' id='eltd_moose_print_stylesheet-css'  href='http://ayni.com.pe/wp-content/themes/moose/css/print.css?ver=4.5.3' type='text/css' media='all' />
      <link rel='stylesheet' id='eltd_moose_blog-css'  href='http://ayni.com.pe/wp-content/themes/moose/css/blog.min.css?ver=4.5.3' type='text/css' media='all' />
      <link rel='stylesheet' id='eltd_moose_style_dynamic-css'  href='http://ayni.com.pe/wp-content/themes/moose/css/style_dynamic.css?ver=1464022484' type='text/css' media='all' />
      <link rel='stylesheet' id='eltd_moose_font_awesome-css'  href='http://ayni.com.pe/wp-content/themes/moose/css/font-awesome/css/font-awesome.min.css?ver=4.5.3' type='text/css' media='all' />
      <link rel='stylesheet' id='eltd_moose_font_elegant-css'  href='http://ayni.com.pe/wp-content/themes/moose/css/elegant-icons/style.min.css?ver=4.5.3' type='text/css' media='all' />
      <link rel='stylesheet' id='eltd_moose_ion_icons-css'  href='http://ayni.com.pe/wp-content/themes/moose/css/ion-icons/css/ionicons.min.css?ver=4.5.3' type='text/css' media='all' />
      <link rel='stylesheet' id='eltd_moose_linea_icons-css'  href='http://ayni.com.pe/wp-content/themes/moose/css/linea-icons/style.css?ver=4.5.3' type='text/css' media='all' />
      <link rel='stylesheet' id='eltd_moose_simple_line_icons-css'  href='http://ayni.com.pe/wp-content/themes/moose/css/simple-line-icons/simple-line-icons.css?ver=4.5.3' type='text/css' media='all' />
      <link rel='stylesheet' id='eltd_moose_dripicons-css'  href='http://ayni.com.pe/wp-content/themes/moose/css/dripicons/dripicons.css?ver=4.5.3' type='text/css' media='all' />
      <link rel='stylesheet' id='eltd_moose_responsive-css'  href='http://ayni.com.pe/wp-content/themes/moose/css/responsive.min.css?ver=4.5.3' type='text/css' media='all' />
      <link rel='stylesheet' id='eltd_moose_style_dynamic_responsive-css'  href='http://ayni.com.pe/wp-content/themes/moose/css/style_dynamic_responsive.css?ver=1464022484' type='text/css' media='all' />
      <link rel='stylesheet' id='eltd_moose_vertical_responsive-css'  href='http://ayni.com.pe/wp-content/themes/moose/css/vertical_responsive.min.css?ver=4.5.3' type='text/css' media='all' />
      <link rel='stylesheet' id='js_composer_front-css'  href='http://ayni.com.pe/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=4.11.2.1' type='text/css' media='all' />
      <link rel='stylesheet' id='eltd_moose_custom_css-css'  href='http://ayni.com.pe/wp-content/themes/moose/css/custom_css.css?ver=1464022484' type='text/css' media='all' />
      <link rel='stylesheet' id='eltd_moose_webkit-css'  href='http://ayni.com.pe/wp-content/themes/moose/css/webkit_stylesheet.css?ver=4.5.3' type='text/css' media='all' />
      <link rel='stylesheet' id='eltd_moose_google_fonts-css'  href='https://fonts.googleapis.com/css?family=Raleway%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMarck+Script%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMontserrat%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CABeeZee%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRaleway%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMarck+Script%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;subset=latin%2Clatin-ext&#038;ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='bsf-Defaults-css'  href='http://ayni.com.pe/wp-content/uploads/smile_fonts/Defaults/Defaults.css?ver=4.5.3' type='text/css' media='all' />
      <link rel='stylesheet' id='uberbox-css'  href='http://ayni.com.pe/wp-content/plugins/awesome-gallery/vendor/uberbox/dist/uberbox.css?ver=1.5.23' type='text/css' media='all' />
      <link rel='stylesheet' id='awesome-gallery-css'  href='http://ayni.com.pe/wp-content/plugins/awesome-gallery/assets/css/awesome-gallery.css?ver=4.5.3' type='text/css' media='all' />
      <script type='text/javascript' src='http://ayni.com.pe/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.1.5'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.1.5'></script>
      <link rel='https://api.w.org/' href='http://ayni.com.pe/wp-json/' />
      <link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://ayni.com.pe/xmlrpc.php?rsd" />
      <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://ayni.com.pe/wp-includes/wlwmanifest.xml" />
      <meta name="generator" content="WordPress 4.5.3" />
      <link rel="canonical" href="http://ayni.com.pe/bio/" />
      <link rel='shortlink' href='http://ayni.com.pe/?p=349021' />
      <link rel="alternate" type="application/json+oembed" href="http://ayni.com.pe/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fayni.com.pe%2Fbio%2F" />
      <link rel="alternate" type="text/xml+oembed" href="http://ayni.com.pe/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fayni.com.pe%2Fbio%2F&#038;format=xml" />
      <!--[if IE 9]>
      <link rel="stylesheet" type="text/css" href="http://ayni.com.pe/wp-content/themes/moose/css/ie9_stylesheet.css" media="screen">
      <![endif]-->      
      <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
      <meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
      <!--[if lte IE 9]>
      <link rel="stylesheet" type="text/css" href="http://ayni.com.pe/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen">
      <![endif]--><!--[if IE  8]>
      <link rel="stylesheet" type="text/css" href="http://ayni.com.pe/wp-content/plugins/js_composer/assets/css/vc-ie8.min.css" media="screen">
      <![endif]-->
      <style type="text/css" media="print">#wpadminbar { display:none; }</style>
      <style type="text/css" media="screen">
         html { margin-top: 32px !important; }
         * html body { margin-top: 32px !important; }
         @media screen and ( max-width: 782px ) {
         html { margin-top: 46px !important; }
         * html body { margin-top: 46px !important; }
         }
      </style>
      <meta name="generator" content="Powered by Slider Revolution 5.1.5 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
      <style type="text/css" data-type="vc_custom-css">h3{
         font-family: gotham !important;
         }
         p{
         font-size: 11.4px !important;
         color: #333333 !important;
         text-transform: uppercase;
         font-family: gotham !important;
         }
         .content_inner  p{

         }
      </style>
      <noscript>
         <style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style>
      </noscript>
      <style>
         /*.uberbox-lightbox-item.uberbox-no-description .uberbox-image-content img {
         top: 36% !important;
         width: 300px !important;
         }*/
      </style>
   </head>
   <script>
      jQuery(function(){
         var shop = '<li id="nav-menu-item-0" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow">'+
      '<a href="http://52.11.161.217/shop/" class="">'+
      '<span class="item_inner">'+
      '<span class="menu_icon_wrapper">'+
      '<i class="menu_icon null fa"></i>'+
      '</span>'+
      '<span class="item_text">Shop</span>'+
      '</span>'+
      '<span class="plus"></span></a>'+
      '</li>';
         //jQuery('#nav-menu-item-348511 a').attr('href','http://wplind.com/ayni/stores/');
         jQuery('#nav-menu-item-349592').after(shop);
      })
   </script>
   <body class="page page-id-349021 page-template-default logged-in admin-bar no-customize-support eltd-cpt-1.0 ajax_fade page_not_loaded  ayni-ver-1.2 vertical_menu_enabled  vertical_menu_left  vertical_menu_width_350 vertical_menu_background_opacity  vertical_menu_with_scroll smooth_scroll side_menu_slide_from_right blog_installed wpb-js-composer js-comp-ver-4.11.2.1 vc_responsive">
      <div class="ajax_loader">
         <div class="ajax_loader_1">
            <div class="pulse"></div>
         </div>
      </div>
      <div class="wrapper">
         <div class="wrapper_inner">
            <aside class="vertical_menu_area with_scroll ">
               <div class="vertical_menu_area_inner">
                  <div class="scroll_area">
                     <div class="vertical_area_background " style="opacity:0;"></div>
                     <div class="vertical_logo_wrapper">
                        <div class="eltd_logo_vertical" style="height: 30px;">
                           <a href="http://ayni.com.pe/">
                           <img class="normal" src="http://wplind.com/wp-content/uploads/2016/02/logo300v2.png" alt="Logo"/>
                           <img class="light" src="http://wplind.com/wp-content/uploads/2016/02/logo300v2.png" alt="Logo"/>
                           <img class="dark" src="http://wplind.com/wp-content/uploads/2016/02/logo300v2.png" alt="Logo"/>
                           </a>
                        </div>
                     </div>
                     <nav class="vertical_menu dropdown_animation vertical_menu_toggle ">
                        <ul id="menu-main-menu" class="">
                           <li id="nav-menu-item-348403" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow">
                              <a href="#" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">Campaign</span></span><span class="plus"></span></a>
                              <div class="second" >
                                 <div class="inner">
                                    <ul >
                                       <li id="nav-menu-item-349041" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub">
                                          <a href="#" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">FW 16</span></span><span class="plus"></span><i class="eltd_menu_arrow fa fa-angle-right"></i></a>
                                          <ul >
                                             <li id="nav-menu-item-348540" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/campaignfw16/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">WOMEN</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-348978" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/campaign-ayni-men/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">MEN</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-349507" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/home-line-lookbook/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">HOME</span></span><span class="plus"></span></a></li>
                                          </ul>
                                       </li>
                                       <li id="nav-menu-item-349214" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/ss-16/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">SS 16</span></span><span class="plus"></span></a></li>
                                       <li id="nav-menu-item-349203" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/campaign-fw-15/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">FW 15</span></span><span class="plus"></span></a></li>
                                    </ul>
                                 </div>
                              </div>
                           </li>
                           <li id="nav-menu-item-348396" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow">
                              <a href="#" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">Collections</span></span><span class="plus"></span></a>
                              <div class="second" >
                                 <div class="inner">
                                    <ul >
                                       <li id="nav-menu-item-349043" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub">
                                          <a href="#" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">FW 16</span></span><span class="plus"></span><i class="eltd_menu_arrow fa fa-angle-right"></i></a>
                                          <ul >
                                             <li id="nav-menu-item-348557" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/fw-16/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">WOMEN</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-349001" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/colletions-ayni-men/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">MEN</span></span><span class="plus"></span></a></li>
                                          </ul>
                                       </li>
                                       <li id="nav-menu-item-348691" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/collections-ss-16/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">SS 16</span></span><span class="plus"></span></a></li>
                                       <li id="nav-menu-item-348602" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/collections-fw-15/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">FW 15</span></span><span class="plus"></span></a></li>
                                       <li id="nav-menu-item-348904" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/ss-15/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">SS 15</span></span><span class="plus"></span></a></li>
                                       <li id="nav-menu-item-349218" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/salesbook/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">SALESBOOK</span></span><span class="plus"></span></a></li>
                                       <li id="nav-menu-item-349514" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/salesbook-de-home-line/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">HOME LINE Salesbook</span></span><span class="plus"></span></a></li>
                                    </ul>
                                 </div>
                              </div>
                           </li>
                           <li id="nav-menu-item-348397" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow">
                              <a href="#" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">Collaborations</span></span><span class="plus"></span></a>
                              <div class="second" >
                                 <div class="inner">
                                    <ul >
                                       <li id="nav-menu-item-349588" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/lama-x-ayni/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">LAMA x AYNI</span></span><span class="plus"></span></a></li>
                                       <li id="nav-menu-item-349589" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/ayni-x-rialtojeanproject/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">AYNI x RIALTOJEANPROJECT</span></span><span class="plus"></span></a></li>
                                       <li id="nav-menu-item-349590" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/juana-burga-x-ayni/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">JUANA BURGA x AYNI</span></span><span class="plus"></span></a></li>
                                    </ul>
                                 </div>
                              </div>
                           </li>
                           <li id="nav-menu-item-349015" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children active has_sub narrow">
                              <a href="http://ayni.com.pe/our-universe/" class=" current "><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">Our universe</span></span><span class="plus"></span></a>
                              <div class="second" >
                                 <div class="inner">
                                    <ul >
                                       <li id="nav-menu-item-349047" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-349021 current_page_item "><a href="http://ayni.com.pe/bio/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">BIO</span></span><span class="plus"></span></a></li>
                                       <li id="nav-menu-item-349046" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/csr/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">CSR</span></span><span class="plus"></span></a></li>
                                       <li id="nav-menu-item-349045" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/alpaca/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">ALPACA</span></span><span class="plus"></span></a></li>
                                       <li id="nav-menu-item-349159" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/press/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">PRESS</span></span><span class="plus"></span></a></li>
                                       <li id="nav-menu-item-349192" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/shows/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">SHOWS</span></span><span class="plus"></span></a></li>
                                    </ul>
                                 </div>
                              </div>
                           </li>
                           <li id="nav-menu-item-349592" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children  has_sub narrow">
                              <a href="http://ayni.com.pe/stores/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">Stores</span></span><span class="plus"></span></a>
                              <div class="second" >
                                 <div class="inner">
                                    <ul >
                                       <li id="nav-menu-item-348694" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub">
                                          <a href="#" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">UNITED STATES</span></span><span class="plus"></span><i class="eltd_menu_arrow fa fa-angle-right"></i></a>
                                          <ul >
                                             <li id="nav-menu-item-348724" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/7-on-locust/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">7 ON LOCUST</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-348727" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/beach-house-brand/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">BEACH HOUSE BRAND</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-348732" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/chrison-bellina/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">CHRISON BELLINA</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-348735" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/corey-co/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">COREY &#038; CO</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-348739" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/details-design/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">DETAILS &#038; DESIGN</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-348767" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/frances-heffernan/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">FRANCES HEFFERNAN</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-348766" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/george/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">GEORGE</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-348765" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/hepburn/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">HEPBURN</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-348764" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/lili-the-first/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">LILI THE FIRST</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-348763" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/maude/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">MAUDE</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-348762" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/mint/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">MINT</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-348761" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/mulberry-me/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">MULBERRY &#038; ME</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-348809" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/mushimina/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">MUSHIMINA</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-348808" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/out-of-hand/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">OUT OF HAND</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-348807" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/penelope/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">PENELOPE</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-348806" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/peper-apprel-parlor-shoes/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">PEPER APPAREL &#038; PARLOR SHOES</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-348805" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/revolve-clothing/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">REVOLVE CLOTHING</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-348804" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/she/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">SHE</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-348803" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/skiffingtons/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">SKIFFINGTONS</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-348802" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/splurge/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">SPLURGE</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-348801" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/style-camp/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">STYLE CAMP</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-348800" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/the-styleliner/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">THE STYLELINER</span></span><span class="plus"></span></a></li>
                                          </ul>
                                       </li>
                                       <li id="nav-menu-item-348813" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub">
                                          <a href="#" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">EUROPE</span></span><span class="plus"></span><i class="eltd_menu_arrow fa fa-angle-right"></i></a>
                                          <ul >
                                             <li id="nav-menu-item-348812" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/willka/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">WILLKA</span></span><span class="plus"></span></a></li>
                                          </ul>
                                       </li>
                                       <li id="nav-menu-item-348815" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub">
                                          <a href="#" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">AUSTRALIA</span></span><span class="plus"></span><i class="eltd_menu_arrow fa fa-angle-right"></i></a>
                                          <ul >
                                             <li id="nav-menu-item-348817" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/husk/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">HUSK</span></span><span class="plus"></span></a></li>
                                          </ul>
                                       </li>
                                       <li id="nav-menu-item-348822" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub">
                                          <a href="#" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">JAPAN</span></span><span class="plus"></span><i class="eltd_menu_arrow fa fa-angle-right"></i></a>
                                          <ul >
                                             <li id="nav-menu-item-348845" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/american-rag-cie-japan/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">AMERICAN RAG CIE JAPAN</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-348844" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/aquagirl/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">AQUAGIRL</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-348843" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/barneys-japan-co-ltd/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">BARNEYS JAPAN CO. LTD.</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-348842" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/itochu/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">ITOCHU</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-348841" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/lourphyli/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">LOURPHYLI</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-348840" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/marubeni/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">MARUBENI</span></span><span class="plus"></span></a></li>
                                          </ul>
                                       </li>
                                       <li id="nav-menu-item-348851" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub">
                                          <a href="#" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">LATIN AMERICA</span></span><span class="plus"></span><i class="eltd_menu_arrow fa fa-angle-right"></i></a>
                                          <ul >
                                             <li id="nav-menu-item-348856" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/atelier-boutique/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">ATELIER BOUTIQUE</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-348855" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/ayni/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">AYNI</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-348857" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/lita-mortari/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">LITA MORTARI</span></span><span class="plus"></span></a></li>
                                          </ul>
                                       </li>
                                       <li id="nav-menu-item-348872" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub">
                                          <a href="#" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">SHOWROOM/AGENTS</span></span><span class="plus"></span><i class="eltd_menu_arrow fa fa-angle-right"></i></a>
                                          <ul >
                                             <li id="nav-menu-item-348874" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/bond-showroom/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">BOND SHOWROOM</span></span><span class="plus"></span></a></li>
                                             <li id="nav-menu-item-348873" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="http://ayni.com.pe/south-pacific-commerce-pty-ltd/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">SOUTH PACIFIC COMMERCE PTY. LTD.</span></span><span class="plus"></span></a></li>
                                          </ul>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                           </li>
                           <li id="nav-menu-item-348915" class="menu-item menu-item-type-post_type menu-item-object-page  narrow"><a href="http://ayni.com.pe/contact-us/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon null fa"></i></span><span class="item_text">Contact</span></span><span class="plus"></span></a></li>
                           <li id="nav-menu-item-349804" class="menu-item menu-item-type-post_type menu-item-object-page  narrow"><a href="http://ayni.com.pe/terminos-y-condiciones/" class=""><span class="item_inner"><span class="menu_icon_wrapper"><i class="menu_icon blank fa"></i></span><span class="item_text">Terms</span></span><span class="plus"></span></a></li>
                        </ul>
                     </nav>
                     <div class="vertical_menu_area_icons_holder">
                     </div>
                     <div class="vertical_menu_area_widget_holder">
                     </div>
                  </div>
               </div>
            </aside>
            <header class="page_header   fixed   ">
               <div class="header_inner clearfix">
                  <div class="header_bottom  header_in_grid clearfix" >
                     <div class="container">
                        <div class="container_inner clearfix" >
                           <div class="header_inner_left">
                              <div class="mobile_menu_button"><span>
                                 <span aria-hidden="true" class="eltd_icon_font_elegant icon_menu " ></span>                           </span>
                              </div>
                              <div class="logo_wrapper">
                                 <div class="eltd_logo"><a href="http://ayni.com.pe/"><img class="normal" src="http://wplind.com/wp-content/uploads/2016/02/logo300v2.png" alt="Logo"/><img class="light" src="http://wplind.com/wp-content/uploads/2016/02/logo300v2.png" alt="Logo"/><img class="dark" src="http://wplind.com/wp-content/uploads/2016/02/logo300v2.png" alt="Logo"/><img class="sticky" src="http://wplind.com/wp-content/uploads/2016/02/logo300v2.png" alt="Logo"/><img class="mobile" src="http://wplind.com/wp-content/uploads/2016/02/logo300v2.png" alt="Logo"/></a></div>
                              </div>
                           </div>
                           <nav class="mobile_menu">
                              <ul id="menu-main-menu-1" class="">
                                 <li id="mobile-menu-item-348403" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub level0">
                                    <a href="#" class=""><span>Campaign</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                    <ul class="sub_menu">
                                       <li id="mobile-menu-item-349041" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub level1">
                                          <a href="#" class=""><span>FW 16</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                          <ul class="sub_menu">
                                             <li id="mobile-menu-item-348540" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/campaignfw16/" class=""><span>WOMEN</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-348978" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/campaign-ayni-men/" class=""><span>MEN</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-349507" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/home-line-lookbook/" class=""><span>HOME</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                          </ul>
                                       </li>
                                       <li id="mobile-menu-item-349214" class="menu-item menu-item-type-post_type menu-item-object-page  level1"><a href="http://ayni.com.pe/ss-16/" class=""><span>SS 16</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                       <li id="mobile-menu-item-349203" class="menu-item menu-item-type-post_type menu-item-object-page  level1"><a href="http://ayni.com.pe/campaign-fw-15/" class=""><span>FW 15</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                    </ul>
                                 </li>
                                 <li id="mobile-menu-item-348396" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub level0">
                                    <a href="#" class=""><span>Collections</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                    <ul class="sub_menu">
                                       <li id="mobile-menu-item-349043" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub level1">
                                          <a href="#" class=""><span>FW 16</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                          <ul class="sub_menu">
                                             <li id="mobile-menu-item-348557" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/fw-16/" class=""><span>WOMEN</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-349001" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/colletions-ayni-men/" class=""><span>MEN</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                          </ul>
                                       </li>
                                       <li id="mobile-menu-item-348691" class="menu-item menu-item-type-post_type menu-item-object-page  level1"><a href="http://ayni.com.pe/collections-ss-16/" class=""><span>SS 16</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                       <li id="mobile-menu-item-348602" class="menu-item menu-item-type-post_type menu-item-object-page  level1"><a href="http://ayni.com.pe/collections-fw-15/" class=""><span>FW 15</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                       <li id="mobile-menu-item-348904" class="menu-item menu-item-type-post_type menu-item-object-page  level1"><a href="http://ayni.com.pe/ss-15/" class=""><span>SS 15</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                       <li id="mobile-menu-item-349218" class="menu-item menu-item-type-post_type menu-item-object-page  level1"><a href="http://ayni.com.pe/salesbook/" class=""><span>SALESBOOK</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                       <li id="mobile-menu-item-349514" class="menu-item menu-item-type-post_type menu-item-object-page  level1"><a href="http://ayni.com.pe/salesbook-de-home-line/" class=""><span>HOME LINE Salesbook</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                    </ul>
                                 </li>
                                 <li id="mobile-menu-item-348397" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub level0">
                                    <a href="#" class=""><span>Collaborations</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                    <ul class="sub_menu">
                                       <li id="mobile-menu-item-349588" class="menu-item menu-item-type-post_type menu-item-object-page  level1"><a href="http://ayni.com.pe/lama-x-ayni/" class=""><span>LAMA x AYNI</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                       <li id="mobile-menu-item-349589" class="menu-item menu-item-type-post_type menu-item-object-page  level1"><a href="http://ayni.com.pe/ayni-x-rialtojeanproject/" class=""><span>AYNI x RIALTOJEANPROJECT</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                       <li id="mobile-menu-item-349590" class="menu-item menu-item-type-post_type menu-item-object-page  level1"><a href="http://ayni.com.pe/juana-burga-x-ayni/" class=""><span>JUANA BURGA x AYNI</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                    </ul>
                                 </li>
                                 <li id="mobile-menu-item-349015" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children active has_sub level0">
                                    <a href="http://ayni.com.pe/our-universe/" class=" current "><span>Our universe</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                    <ul class="sub_menu">
                                       <li id="mobile-menu-item-349047" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-349021 current_page_item active level1"><a href="http://ayni.com.pe/bio/" class=""><span>BIO</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                       <li id="mobile-menu-item-349046" class="menu-item menu-item-type-post_type menu-item-object-page  level1"><a href="http://ayni.com.pe/csr/" class=""><span>CSR</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                       <li id="mobile-menu-item-349045" class="menu-item menu-item-type-post_type menu-item-object-page  level1"><a href="http://ayni.com.pe/alpaca/" class=""><span>ALPACA</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                       <li id="mobile-menu-item-349159" class="menu-item menu-item-type-post_type menu-item-object-page  level1"><a href="http://ayni.com.pe/press/" class=""><span>PRESS</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                       <li id="mobile-menu-item-349192" class="menu-item menu-item-type-post_type menu-item-object-page  level1"><a href="http://ayni.com.pe/shows/" class=""><span>SHOWS</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                    </ul>
                                 </li>
                                 <li id="mobile-menu-item-349592" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children  has_sub level0">
                                    <a href="http://ayni.com.pe/stores/" class=""><span>Stores</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                    <ul class="sub_menu">
                                       <li id="mobile-menu-item-348694" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub level1">
                                          <a href="#" class=""><span>UNITED STATES</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                          <ul class="sub_menu">
                                             <li id="mobile-menu-item-348724" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/7-on-locust/" class=""><span>7 ON LOCUST</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-348727" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/beach-house-brand/" class=""><span>BEACH HOUSE BRAND</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-348732" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/chrison-bellina/" class=""><span>CHRISON BELLINA</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-348735" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/corey-co/" class=""><span>COREY &#038; CO</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-348739" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/details-design/" class=""><span>DETAILS &#038; DESIGN</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-348767" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/frances-heffernan/" class=""><span>FRANCES HEFFERNAN</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-348766" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/george/" class=""><span>GEORGE</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-348765" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/hepburn/" class=""><span>HEPBURN</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-348764" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/lili-the-first/" class=""><span>LILI THE FIRST</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-348763" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/maude/" class=""><span>MAUDE</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-348762" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/mint/" class=""><span>MINT</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-348761" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/mulberry-me/" class=""><span>MULBERRY &#038; ME</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-348809" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/mushimina/" class=""><span>MUSHIMINA</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-348808" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/out-of-hand/" class=""><span>OUT OF HAND</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-348807" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/penelope/" class=""><span>PENELOPE</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-348806" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/peper-apprel-parlor-shoes/" class=""><span>PEPER APPAREL &#038; PARLOR SHOES</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-348805" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/revolve-clothing/" class=""><span>REVOLVE CLOTHING</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-348804" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/she/" class=""><span>SHE</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-348803" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/skiffingtons/" class=""><span>SKIFFINGTONS</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-348802" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/splurge/" class=""><span>SPLURGE</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-348801" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/style-camp/" class=""><span>STYLE CAMP</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-348800" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/the-styleliner/" class=""><span>THE STYLELINER</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                          </ul>
                                       </li>
                                       <li id="mobile-menu-item-348813" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub level1">
                                          <a href="#" class=""><span>EUROPE</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                          <ul class="sub_menu">
                                             <li id="mobile-menu-item-348812" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/willka/" class=""><span>WILLKA</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                          </ul>
                                       </li>
                                       <li id="mobile-menu-item-348815" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub level1">
                                          <a href="#" class=""><span>AUSTRALIA</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                          <ul class="sub_menu">
                                             <li id="mobile-menu-item-348817" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/husk/" class=""><span>HUSK</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                          </ul>
                                       </li>
                                       <li id="mobile-menu-item-348822" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub level1">
                                          <a href="#" class=""><span>JAPAN</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                          <ul class="sub_menu">
                                             <li id="mobile-menu-item-348845" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/american-rag-cie-japan/" class=""><span>AMERICAN RAG CIE JAPAN</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-348844" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/aquagirl/" class=""><span>AQUAGIRL</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-348843" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/barneys-japan-co-ltd/" class=""><span>BARNEYS JAPAN CO. LTD.</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-348842" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/itochu/" class=""><span>ITOCHU</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-348841" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/lourphyli/" class=""><span>LOURPHYLI</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-348840" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/marubeni/" class=""><span>MARUBENI</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                          </ul>
                                       </li>
                                       <li id="mobile-menu-item-348851" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub level1">
                                          <a href="#" class=""><span>LATIN AMERICA</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                          <ul class="sub_menu">
                                             <li id="mobile-menu-item-348856" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/atelier-boutique/" class=""><span>ATELIER BOUTIQUE</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-348855" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/ayni/" class=""><span>AYNI</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-348857" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/lita-mortari/" class=""><span>LITA MORTARI</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                          </ul>
                                       </li>
                                       <li id="mobile-menu-item-348872" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub level1">
                                          <a href="#" class=""><span>SHOWROOM/AGENTS</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
                                          <ul class="sub_menu">
                                             <li id="mobile-menu-item-348874" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/bond-showroom/" class=""><span>BOND SHOWROOM</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                             <li id="mobile-menu-item-348873" class="menu-item menu-item-type-post_type menu-item-object-page  level2"><a href="http://ayni.com.pe/south-pacific-commerce-pty-ltd/" class=""><span>SOUTH PACIFIC COMMERCE PTY. LTD.</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                          </ul>
                                       </li>
                                    </ul>
                                 </li>
                                 <li id="mobile-menu-item-348915" class="menu-item menu-item-type-post_type menu-item-object-page  level0"><a href="http://ayni.com.pe/contact-us/" class=""><span>Contact</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                                 <li id="mobile-menu-item-349804" class="menu-item menu-item-type-post_type menu-item-object-page  level0"><a href="http://ayni.com.pe/terminos-y-condiciones/" class=""><span>Terms</span></a><span class="mobile_arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span></li>
                              </ul>
                           </nav>
                        </div>
                     </div>
                  </div>
               </div>
            </header>
            <a id='back_to_top' class="" href='#'>
            <span class="eltd_icon_stack">
            <span aria-hidden="true" class="eltd_icon_font_elegant arrow_carrot-up  " ></span>        </span>
            </a>
            <div class="content content_top_margin">
               <div class="meta">
                  <div class="seo_title">AYNI | Bio</div>
                  <span id="eltd_page_id">349021</span>
                  <div class="body_classes">page,page-id-349021,page-template-default,logged-in,admin-bar,no-customize-support,eltd-cpt-1.0,ajax_fade,page_not_loaded,,ayni-ver-1.2,vertical_menu_enabled, vertical_menu_left, vertical_menu_width_350,vertical_menu_background_opacity, vertical_menu_with_scroll,smooth_scroll,side_menu_slide_from_right,blog_installed,wpb-js-composer js-comp-ver-4.11.2.1,vc_responsive</div>
               </div>
               <div class="content_inner  ">
                  <style type="text/css" data-type="vc_custom-css-349021" scoped>h3{
                     font-family: gotham !important;
                     }
                     p{
                     font-size: 11.4px !important;
                     color: #333333 !important;
                     text-transform: uppercase;
                     font-family: gotham !important;
                     }
                  </style>
                  <script>
                     page_scroll_amount_for_sticky = undefined
                  </script>
                  <div class="container">
                     <div class="container_inner default_template_holder clearfix" >
                        <div      class="vc_row wpb_row section grid_section" style=' text-align:left;'>
                           <div class=" section_inner clearfix">
                              <div class='section_inner_margin clearfix'>
                                 <div class="wpb_column vc_column_container vc_col-sm-3">
                                    <div class="vc_column-inner ">
                                       <div class="wpb_wrapper"></div>
                                    </div>
                                 </div>
                                 <div class="wpb_column vc_column_container vc_col-sm-6">
                                    <div class="vc_column-inner ">
                                       <div class="wpb_wrapper">
                                          <div class="wpb_text_column wpb_content_element ">
                                             <div class="wpb_wrapper">
                                                <h3 style="text-align: center;">COSTUMER SERVICE</h3>
                                             </div>
                                          </div>
                                          <div class="vc_empty_space"  style="height: 32px" ><span class="vc_empty_space_inner"><span class="empty_space_image"
                                             ></span>
                                             </span>
                                          </div>
                                          <div class="wpb_text_column wpb_content_element ">
                                             <div class="wpb_wrapper">
                                                <p>
                                                   <strong>SHIPPING</strong><br>
                                                   ALL ORDERS ARE SHIPPED MONDAY - FRIDAY, EXCLUDING HOLIDAYS. EXPRESS ORDERS INCLUDING 

                                                   2ND DAY AND OVERNIGHT PLACED MONDAY - FRIDAY AFTER 12 PM EASTERN TIME WILL BE

                                                   PROCESSED FOLLOWING BUSINESS DAY. EXPRESS ORDERS PLACED SATURDAY - SUNDAY WILL BE

                                                   PROCESSED MONDAY.
                                                </p>
                                                <p>&nbsp;</p>
                                                <p>
                                                   ONCE YOUR CREDIT CARD IS AUTHORIZED, YOU WILL RECEIVE A CONFIRMATION EMAIL. ORDERS ARE

                                                   PROCESSED WITHIN 2-3 BUSINESS DAYS. ONCE PROCESSED YOU WILL RECEIVE YOUR ORDER WITHIN

                                                   3-10 BUSINESS DAYS.
                                                </p>
                                                <p>
                                                   AYNI IS NOT RESPONSIBLE FOR DELAYS IN SHIPPING OR DELIVERY DUE TO FORCE OF NATURE OR

                                                   OTHER UNCONTROLLABLE EVENTS.
                                                </p>
                                                <p>
                                                   <strong>APROXIMATE SHIPPING RATES</strong><br>

                                                   GROUND $15.00<br>

                                                   2ND DAY $35.00<br>

                                                   OVERNIGHT $45.00
                                                </p>
                                                <p>&nbsp;</p>
                                                <p>
                                                      <strong>PAYMENT</strong>
                                                </p>
                                                <ul>
                                                   <li>WE ACCEPT VISA®, MASTERCARD®, AND AMERICAN EXPRESS®.</li>
                                                   <li>PAYPAL® IS AVAILABLE FOR PURCHASES MADE IN $USD. WE CANNOT ACCEPT CHECKS, CASH OR MONEY ORDERS.</li>
                                                </ul>

                                                <p>
                                                      <strong>PRE-ORDER</strong>
                                                </p>
                                                <ul>
                                                   <li>
                                                         FULL PAYMENT IS REQUIRED AT THE TIME OF PURCHASE. WE ARE HAPPY TO MAKE ANY

                                                         NECESSARY REVISIONS OR CANCEL AN ORDER FOR A FULL REFUND IF NECESSARY PRIOR TO

                                                         SHIPMENT.
                                                   </li>
                                                </ul>

                                                <p>
                                                      <strong>LEGAL PURCHASE AGE</strong>
                                                </p>
                                                <ul>
                                                   <li>
                                                         IF YOU ARE UNDER EIGHTEEN (18), WE REQUIRE THAT YOU INFORM AND GET YOUR PARENTS OR

                                                         GUARDIANS CONSENT BEFORE PURCHASING ANYTHING ON ACNESTUDIOS.COM OR ANY OTHER

                                                         WEBSITE RELATED TO AYNI.
                                                   </li>
                                                </ul>

                                             </div>
                                          </div>
                                          <div class="wpb_text_column wpb_content_element ">
                                             <div class="wpb_wrapper">
                                                <h3 style="text-align: center;">PRIVACY POLICY</h3>
                                             </div>
                                          </div>
                                          <div class="wpb_text_column wpb_content_element ">
                                             <div class="wpb_wrapper">
                                                <p>
                                                   <strong>ACCEPTANCE OF PRIVACY AND MODIFICATIONS</strong><br>

                                                   BY ACCESSING AND USING AYNI WEBSITE (INCLUDING PURCHASING ITEMS THROUGH OUR WEBSITE),

                                                   OR SUBMITTING A PERSONAL INFORMATION, YOU CONSENT TO OUR COLLECTION, USE AND

                                                   DISCLOSURE OF YOUR INFORMATION AS DESCRIBED IN THIS PRIVACY POLICY.
                                                </p>
                                                <p>
                                                   AYNI RESERVES THE RIGHT OCCASIONALLY TO MAKE CHANGES TO ITS PRIVACY POLICY OR

                                                   PRACTICES. WE WILL POST THE UPDATED POLICY ON OUR WEBSITE, AND THUS WE ENCOURAGE YOU

                                                   TO REVIEW THIS PAGE FROM TIME TO TIME, PARTICULARLY EACH TIME YOU USE OUR ONLINE SHOP.

                                                   LAST DATE FOR MODIFICATION: JUNE 13TH
                                                </p>
                                                <p>
                                                   PERSONAL DATA WILL BE DISCLOSED TO THIRD PARTY COMPANIES THAT PROVIDE, ON BEHALF OF

                                                   AYNI, SPECIFIC SERVICES AS DATA PROCESSORS OR TO OTHER RECIPIENTS OF PERSONAL DATA

                                                   COLLECTED BY US.
                                                </p>
                                                <p>
                                                   WE WISH TO INFORM YOU THAT US MAY PROCESS YOUR PERSONAL DATA ALSO WITHOUT YOUR

                                                   CONSENT IN CERTAIN CIRCUMSTANCES PROVIDED BY LAWS.
                                                </p>
                                                <p>
                                                   <strong>CONTACT US</strong><br>
                                                   IF YOU HAVE ANY COMMENTS OR QUERIES REGARDING OUR PRIVACY POLICY, PLEASE SEND AN

                                                   EMAIL TO <strong>CONTACT@AAYNIDESIGNLAB.COM.</strong>
                                                </p>
                                             </div>
                                          </div>
                                          <div class="wpb_text_column wpb_content_element ">
                                             <div class="wpb_wrapper">
                                                <h3 style="text-align: center;">RETURN POLICY</h3>
                                             </div>
                                          </div>
                                          <div class="wpb_text_column wpb_content_element ">
                                             <div class="wpb_wrapper">

                                                <p>
                                                   <strong>LEGAL PURCHASE AGE</strong>
                                                </p>
                                                <ul>
                                                   <li>
                                                      EMAIL CONTACT@AYNIDESIGNLAB.COM TO RECEIVE A RETURN AUTHORIZATION NUMBER. YOU WILL BE CONTACTED BY CUSTOMER SERVICE WITHIN 48 HOURS, MONDAY - FRIDAY, 9AM - 6PM EST.
                                                   </li>
                                                   <li>
                                                      SECURELY PACK MERCHANDISE WITH RETURN AUTHORIZATION FORM AND MARK THE RETURN AUTHORIZATION NUMBER ON OUTSIDE OF BOX
                                                   </li>
                                                </ul>
                                                <p>
                                                      <strong>RETURNS/EXCHANGES</strong><br>
                                                      IF YOU ARE NOT COMPLETELY SATISFIED WITH YOUR AYNI ONLINE PURCHASE, YOU MAY SEND IT

                                                      BACK TO US FOR AN EXCHANGE OR REFUND. SALE MERCHANDISE. NO EXCHANGES OR RETURNS WILL

                                                      BE ACCEPTED.
                                                </p>
                                                <p>
                                                      <strong>REQUIREMENTS</strong>
                                                </p>
                                                <ul>
                                                   <li>
                                                      ALL MERCHANDISE RETURNED MUST BE UNWORN, UNWASHED, UNMODIFIED, UNDAMAGED WITH PRICE TAGS INTACT.
                                                   </li>
                                                   <li>
                                                      THE PRODUCTS ARE RETURNED IN THEIR ORIGINAL PACKAGING.
                                                   </li>
                                                   <li>
                                                      RETURNS MUST BE SHIPPED WITH TRACKING NUMBER.
                                                   </li>
                                                   <li>
                                                      EXCHANGES/RETURNS MUST BE RECEIVED WITHIN 14 DAYS OF THE DELIVERY DATE.
                                                   </li>
                                                   <li>
                                                      EXCHANGES/REFUNDS ARE PROCESSED WITHIN 7 DAYS AFTER WE RECEIVE THE MERCHAN-DISE. YOU WILL RECEIVE A CONFIRMATION EMAIL WHEN YOUR EXCHANGES/CREDITS ARE PRO-CESSED.
                                                   </li>
                                                </ul>

                                                <p>
                                                   <strong>REFUNDS</strong>
                                                </p>
                                                <p>
                                                   YOU WILL BE NOTIFIED IF THE RETURNED PRODUCTS CANNOT BE ACCEPTED. IN THIS CASE, YOU MAY

                                                   CHOOSE TO HAVE THE PRODUCTS DELIVERED TO YOU AT NO EXPENSE TO THE VENDOR. IF YOU

                                                   REFUSE THE ABOVE DELIVERY, THE VENDOR RESERVES THE RIGHT TO RETAIN THE PRODUCTS AND

                                                   THE AMOUNT PAID FOR YOUR PURCHASE OF THE PRODUCTS.
                                                </p>

                                                <p>
                                                   IF YOU HAVE FULFILLED ALL REQUIREMENTS SET FORTH ABOVE, THE VENDOR SHALL REFUND THE

                                          ENTIRE PRICE FOR THE RETURNED PURCHASED PRODUCTS. ANY DUTIES, TAXES AND FEES YOU HAVE

                                          PAID FOR THE DELIVERY OF THE PURCHASED PRODUCTS SHALL NOT BE REFUNDED. WE DO NOT

                                          REFUND SHIPPING COSTS. SHIPPING COSTS WILL ONLY BE REFUNDED WHEN THE RETURN IS A RESULT

                                          OF AN ERROR OR DAMAGED MERCHANDISE.
                                                </p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="wpb_column vc_column_container vc_col-sm-3">
                                    <div class="vc_column-inner ">
                                       <div class="wpb_wrapper"></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- close div.content_inner -->
            </div>
            <!-- close div.content -->
            <footer class="footer_border_columns">
               <div class="footer_inner clearfix">
               </div>
            </footer>
         </div>
         <!-- close div.wrapper_inner  -->
      </div>
      <!-- close div.wrapper -->
      <script> 
         jQuery(document).ready(function() {       
            
            if (jQuery('#wp-admin-bar-revslider-default').length>0 && jQuery('.rev_slider_wrapper').length>0) {
               var aliases = new Array();
               jQuery('.rev_slider_wrapper').each(function() {
                  aliases.push(jQuery(this).data('alias'));
               });                        
               if    (aliases.length>0)   
                  jQuery('#wp-admin-bar-revslider-default li').each(function() {
                     var li = jQuery(this),
                        t = jQuery.trim(li.find('.ab-item .rs-label').data('alias')); //text()
                        
                     if (jQuery.inArray(t,aliases)!=-1) {
                     } else {
                        li.remove();
                     }
                  });
            } else {
               jQuery('#wp-admin-bar-revslider').remove();
            }
         });
      </script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-includes/js/jquery/ui/widget.min.js?ver=1.11.4'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-includes/js/jquery/ui/mouse.min.js?ver=1.11.4'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-includes/js/jquery/ui/resizable.min.js?ver=1.11.4'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-includes/js/jquery/ui/draggable.min.js?ver=1.11.4'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-includes/js/jquery/ui/button.min.js?ver=1.11.4'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-includes/js/jquery/ui/position.min.js?ver=1.11.4'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-includes/js/jquery/ui/dialog.min.js?ver=1.11.4'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-includes/js/wpdialog.min.js?ver=4.5.3'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-includes/js/admin-bar.min.js?ver=4.5.3'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.51.0-2014.06.20'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var _wpcf7 = {"loaderUrl":"http:\/\/ayni.com.pe\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","recaptchaEmpty":"Please verify that you are not a robot.","sending":"Sending ..."};
         /* ]]> */
      </script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=4.4.2'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var eltdLike = {"ajaxurl":"http:\/\/ayni.com.pe\/wp-admin\/admin-ajax.php"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-content/themes/moose/js/eltd-like.js?ver=1.0'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-content/themes/moose/js/plugins.js?ver=4.5.3'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-content/themes/moose/js/jquery.carouFredSel-6.2.1.js?ver=4.5.3'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-content/themes/moose/js/jquery.fullPage.min.js?ver=4.5.3'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-content/themes/moose/js/lemmon-slider.js?ver=4.5.3'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-content/themes/moose/js/jquery.mousewheel.min.js?ver=4.5.3'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-content/themes/moose/js/jquery.touchSwipe.min.js?ver=4.5.3'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-content/plugins/js_composer/assets/lib/bower/isotope/dist/isotope.pkgd.min.js?ver=4.11.2.1'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-content/themes/moose/js/typed.js?ver=4.5.3'></script>
      <script type='text/javascript' src='//maps.googleapis.com/maps/api/js?ver=4.5.3'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var no_ajax_obj = {"no_ajax_pages":["","http:\/\/ayni.com.pe\/wp-login.php?action=logout&_wpnonce=f83d6cbf47"]};
         /* ]]> */
      </script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-content/themes/moose/js/default_dynamic.js?ver=1464022484'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-content/themes/moose/js/default.min.js?ver=4.5.3'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-content/themes/moose/js/blog.min.js?ver=4.5.3'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-content/themes/moose/js/custom_js.js?ver=1464022484'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-content/themes/moose/js/TweenLite.min.js?ver=4.5.3'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-content/themes/moose/js/ScrollToPlugin.min.js?ver=4.5.3'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-content/themes/moose/js/smoothPageScroll.js?ver=4.5.3'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-includes/js/comment-reply.min.js?ver=4.5.3'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-content/themes/moose/js/ajax.min.js?ver=4.5.3'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js?ver=4.11.2.1'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-includes/js/underscore.min.js?ver=1.8.3'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-includes/js/backbone.min.js?ver=1.2.3'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-content/plugins/awesome-gallery/vendor/backbone.marionette.js?ver=1.5.23'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-content/plugins/awesome-gallery/vendor/uberbox/dist/templates.js?ver=1.5.23'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-content/plugins/awesome-gallery/vendor/uberbox/dist/uberbox.js?ver=1.5.23'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-content/plugins/awesome-gallery/assets/js/awesome-gallery.js?ver=1.5.23'></script>
      <script type='text/javascript' src='http://ayni.com.pe/wp-includes/js/wp-embed.min.js?ver=4.5.3'></script>
      <script type="text/javascript">
         (function() {
            var request, b = document.body, c = 'className', cs = 'customize-support', rcs = new RegExp('(^|\\s+)(no-)?'+cs+'(\\s+|$)');
         
            request = true;
         
            b[c] = b[c].replace( rcs, ' ' );
            b[c] += ( window.postMessage && request ? ' ' : ' no-' ) + cs;
         }());
      </script>
      <div id="wpadminbar" class="nojq nojs">
         <a class="screen-reader-shortcut" href="#wp-toolbar" tabindex="1">Skip to toolbar</a>
         <div class="quicklinks" id="wp-toolbar" role="navigation" aria-label="Toolbar" tabindex="0">
            <ul id="wp-admin-bar-root-default" class="ab-top-menu">
               <li id="wp-admin-bar-wp-logo" class="menupop">
                  <a class="ab-item"  aria-haspopup="true" href="http://ayni.com.pe/wp-admin/about.php"><span class="ab-icon"></span><span class="screen-reader-text">About WordPress</span></a>
                  <div class="ab-sub-wrapper">
                     <ul id="wp-admin-bar-wp-logo-default" class="ab-submenu">
                        <li id="wp-admin-bar-about"><a class="ab-item"  href="http://ayni.com.pe/wp-admin/about.php">About WordPress</a>     </li>
                     </ul>
                     <ul id="wp-admin-bar-wp-logo-external" class="ab-sub-secondary ab-submenu">
                        <li id="wp-admin-bar-wporg"><a class="ab-item"  href="https://wordpress.org/">WordPress.org</a>    </li>
                        <li id="wp-admin-bar-documentation"><a class="ab-item"  href="https://codex.wordpress.org/">Documentation</a>     </li>
                        <li id="wp-admin-bar-support-forums"><a class="ab-item"  href="https://wordpress.org/support/">Support Forums</a>    </li>
                        <li id="wp-admin-bar-feedback"><a class="ab-item"  href="https://wordpress.org/support/forum/requests-and-feedback">Feedback</a>    </li>
                     </ul>
                  </div>
               </li>
               <li id="wp-admin-bar-site-name" class="menupop">
                  <a class="ab-item"  aria-haspopup="true" href="http://ayni.com.pe/wp-admin/">AYNI</a>
                  <div class="ab-sub-wrapper">
                     <ul id="wp-admin-bar-site-name-default" class="ab-submenu">
                        <li id="wp-admin-bar-dashboard"><a class="ab-item"  href="http://ayni.com.pe/wp-admin/">Dashboard</a>    </li>
                     </ul>
                     <ul id="wp-admin-bar-appearance" class="ab-submenu">
                        <li id="wp-admin-bar-themes"><a class="ab-item"  href="http://ayni.com.pe/wp-admin/themes.php">Themes</a>      </li>
                        <li id="wp-admin-bar-widgets"><a class="ab-item"  href="http://ayni.com.pe/wp-admin/widgets.php">Widgets</a>      </li>
                        <li id="wp-admin-bar-menus"><a class="ab-item"  href="http://ayni.com.pe/wp-admin/nav-menus.php">Menus</a>     </li>
                     </ul>
                  </div>
               </li>
               <li id="wp-admin-bar-customize" class="hide-if-no-customize"><a class="ab-item"  href="http://ayni.com.pe/wp-admin/customize.php?url=http%3A%2F%2Fayni.com.pe%2Fbio%2F">Customize</a>     </li>
               <li id="wp-admin-bar-updates"><a class="ab-item"  href="http://ayni.com.pe/wp-admin/update-core.php" title="2 Plugin Updates"><span class="ab-icon"></span><span class="ab-label">2</span><span class="screen-reader-text">2 Plugin Updates</span></a>      </li>
               <li id="wp-admin-bar-comments"><a class="ab-item"  href="http://ayni.com.pe/wp-admin/edit-comments.php"><span class="ab-icon"></span><span id="ab-awaiting-mod" class="ab-label awaiting-mod pending-count count-0" aria-hidden="true">0</span><span class="screen-reader-text">0 comments awaiting moderation</span></a>     </li>
               <li id="wp-admin-bar-new-content" class="menupop">
                  <a class="ab-item"  aria-haspopup="true" href="http://ayni.com.pe/wp-admin/post-new.php"><span class="ab-icon"></span><span class="ab-label">New</span></a>
                  <div class="ab-sub-wrapper">
                     <ul id="wp-admin-bar-new-content-default" class="ab-submenu">
                        <li id="wp-admin-bar-new-post"><a class="ab-item"  href="http://ayni.com.pe/wp-admin/post-new.php">Post</a>    </li>
                        <li id="wp-admin-bar-new-media"><a class="ab-item"  href="http://ayni.com.pe/wp-admin/media-new.php">Media</a>    </li>
                        <li id="wp-admin-bar-new-page"><a class="ab-item"  href="http://ayni.com.pe/wp-admin/post-new.php?post_type=page">Page</a>    </li>
                        <li id="wp-admin-bar-new-portfolio_page"><a class="ab-item"  href="http://ayni.com.pe/wp-admin/post-new.php?post_type=portfolio_page">Portfolio Item</a>    </li>
                        <li id="wp-admin-bar-new-testimonials"><a class="ab-item"  href="http://ayni.com.pe/wp-admin/post-new.php?post_type=testimonials">Testimonial</a>     </li>
                        <li id="wp-admin-bar-new-carousels"><a class="ab-item"  href="http://ayni.com.pe/wp-admin/post-new.php?post_type=carousels">Carousel Item</a>      </li>
                        <li id="wp-admin-bar-new-slides"><a class="ab-item"  href="http://ayni.com.pe/wp-admin/post-new.php?post_type=slides">Slide</a>     </li>
                        <li id="wp-admin-bar-new-masonry_gallery"><a class="ab-item"  href="http://ayni.com.pe/wp-admin/post-new.php?post_type=masonry_gallery">Masonry Gallery Item</a>     </li>
                        <li id="wp-admin-bar-new-awesome-gallery"><a class="ab-item"  href="http://ayni.com.pe/wp-admin/post-new.php?post_type=awesome-gallery">Awesome Gallery</a>    </li>
                        <li id="wp-admin-bar-new-user"><a class="ab-item"  href="http://ayni.com.pe/wp-admin/user-new.php">User</a>    </li>
                     </ul>
                  </div>
               </li>
               <li id="wp-admin-bar-edit"><a class="ab-item"  href="http://ayni.com.pe/wp-admin/post.php?post=349021&#038;action=edit">Edit Page</a>     </li>
               <li id="wp-admin-bar-revslider" class="menupop revslider-menu">
                  <a class="ab-item"  aria-haspopup="true" href="http://ayni.com.pe/wp-admin/admin.php?page=revslider"><span class="rs-label">Slider Revolution</span></a>
                  <div class="ab-sub-wrapper">
                     <ul id="wp-admin-bar-revslider-default" class="ab-submenu">
                        <li id="wp-admin-bar-homeslider_fullscreen" class="revslider-sub-menu"><a class="ab-item"  href="http://ayni.com.pe/wp-admin/admin.php?page=revslider&#038;view=slide&#038;id=new&#038;slider=1"><span class="rs-label" data-alias="homeslider_fullscreen">Homeslider Fullscreen</span></a>     </li>
                     </ul>
                  </div>
               </li>
            </ul>
            <ul id="wp-admin-bar-top-secondary" class="ab-top-secondary ab-top-menu">
               <li id="wp-admin-bar-search" class="admin-bar-search">
                  <div class="ab-item ab-empty-item" tabindex="-1">
                     <form action="http://ayni.com.pe/" method="get" id="adminbarsearch"><input class="adminbar-input" name="s" id="adminbar-search" type="text" value="" maxlength="150" /><label for="adminbar-search" class="screen-reader-text">Search</label><input type="submit" class="adminbar-button" value="Search"/></form>
                  </div>
               </li>
               <li id="wp-admin-bar-my-account" class="menupop with-avatar">
                  <a class="ab-item"  aria-haspopup="true" href="http://ayni.com.pe/wp-admin/profile.php">Howdy, admin<img alt='' src='http://1.gravatar.com/avatar/7031dfa6a740ceea4fc4a14beb3b7a0c?s=26&#038;d=mm&#038;r=g' srcset='http://1.gravatar.com/avatar/7031dfa6a740ceea4fc4a14beb3b7a0c?s=52&amp;d=mm&amp;r=g 2x' class='avatar avatar-26 photo' height='26' width='26' /></a>
                  <div class="ab-sub-wrapper">
                     <ul id="wp-admin-bar-user-actions" class="ab-submenu">
                        <li id="wp-admin-bar-user-info"><a class="ab-item" tabindex="-1" href="http://ayni.com.pe/wp-admin/profile.php"><img alt='' src='http://1.gravatar.com/avatar/7031dfa6a740ceea4fc4a14beb3b7a0c?s=64&#038;d=mm&#038;r=g' srcset='http://1.gravatar.com/avatar/7031dfa6a740ceea4fc4a14beb3b7a0c?s=128&amp;d=mm&amp;r=g 2x' class='avatar avatar-64 photo' height='64' width='64' /><span class='display-name'>admin</span></a>     </li>
                        <li id="wp-admin-bar-edit-profile"><a class="ab-item"  href="http://ayni.com.pe/wp-admin/profile.php">Edit My Profile</a>     </li>
                        <li id="wp-admin-bar-logout"><a class="ab-item"  href="http://ayni.com.pe/wp-login.php?action=logout&#038;_wpnonce=f83d6cbf47">Log Out</a>      </li>
                     </ul>
                  </div>
               </li>
            </ul>
         </div>
         <a class="screen-reader-shortcut" href="http://ayni.com.pe/wp-login.php?action=logout&#038;_wpnonce=f83d6cbf47">Log Out</a>
      </div>
      <script>
         jQuery('body').on('keydown', function(e){
         if(e.which == 37 || e.which == 38 || e.which == 39 || e.which == 40) {
         
         setTimeout(function(){
         
         jQuery('.leyenda-text').remove()
         
         var href = jQuery('.uberbox-current .uberbox-image-content img').attr('src'),
                info = jQuery('.asg-lightbox[href="'+href+'"]').find('.asg-image-caption-wrapper').html();
         //alert(info);
            jQuery('.uberbox-image-content img[src="'+href+'"]').after('<div class="leyenda-text" style="position: absolute;max-width: 100%;max-height: 100%; left: 72%; top: 60%; font-family: gotham; font-size: 14px;">'+info+'</div>');
         
         },250);
         
         }
         
         });
         /*jQuery(window).load(function(){
            //jQuery('div.asg-image.asg-loaded:gt(2)').attr('style', 'left: 0px; top: 500px !important; width: 319px; height: 398px;');
            //jQuery('div.asg-image.asg-loaded:gt(2)').attr('style', '');
         
            setTimeout(function(){
               var i = 0,
                   j = 0,
                   k = 0,
                   acumulaHeight = 0,
                   altoCalculado = jQuery('.asg-image-caption-wrapper:eq(1)').height() + 25;
         
               jQuery('div.asg-image').each(function() {
                  var alto = jQuery(this).height();
                  jQuery(this).height(parseInt(alto+altoCalculado));
                  
                  j++;
                  if (i > 2) {
                     var top = jQuery(this).position().top,
                        left = jQuery(this).position().left,
                        width = jQuery(this).width(),
                        height = jQuery(this).height();
                     //alert(top + ' - ' + left + ' - ' + width + ' - ' + height);
                     //jQuery(this).remove();
         
                     var newTop = parseInt(top) + parseInt(altoCalculado*k);
         
                     jQuery(this).attr('style', 'left: '+ left +'px; top: '+ newTop +'px; width: '+ width +'px; height: '+ height +'px;');
                  }
         
                  if (j == 3) {
                     j = 0;
                     k++;
                     acumulaHeight+= k;
                     //alert(k);
                  }
                  i++;
               });
         
               var heightContainer = jQuery('.asg-images').height(),
                   widthContainer = jQuery('.asg-images').width();
         
               acumulaHeight = acumulaHeight*32;
         
               //alert(heightContainer + ' - ' + widthContainer);
         //alert(acumulaHeight);
         
               if (altoCalculado > 20) {
                  jQuery('.asg-images').attr('style', 'height: '+parseInt(heightContainer+acumulaHeight)+'px; width: '+widthContainer+'px');
               }
         
               else {
                  jQuery('.asg-images').attr('style', 'height: '+parseInt(heightContainer+120)+'px; width: '+widthContainer+'px');
               }
            }, 500);
         });*/
         
         
         jQuery(document).on('click', '.asg-lightbox', function() {
            var slug = jQuery(this).parent().attr('data-slug'),
                info = jQuery(this).find('.asg-image-caption-wrapper').html(),
                href = jQuery(this).attr('href');
         
            jQuery('.leyenda-text').remove()
            //jQuery('.awesome-gallery-'+slug+'-1').html();
            jQuery('.uberbox-image-content img[src="'+href+'"]').after('<div class="leyenda-text" style="position: absolute;max-width: 100%;max-height: 100%; left: 72%; top: 60%; font-family: gotham; font-size: 14px;">'+info+'</div>');
         //alert(info+" - "+href);
         });
         
         jQuery(document).on('click', '.uberbox-prev, .uberbox-next', function() {
            var href = jQuery('.uberbox-current .uberbox-image-content img').attr('src'),
                info = jQuery('.asg-lightbox[href="'+href+'"]').find('.asg-image-caption-wrapper').html();
         //alert(info);
            jQuery('.leyenda-text').remove()
            jQuery('.uberbox-image-content img[src="'+href+'"]').after('<div class="leyenda-text" style="position: absolute;max-width: 100%;max-height: 100%; left: 72%; top: 60%; font-family: gotham; font-size: 14px;">'+info+'</div>');
         });
         
         /*jQuery('a.asg-image-wrapper.asg-lightbox img').each(function() {
            var href = jQuery(this).parent().attr('href');
         console.log(href);
            jQuery(this).attr('src', href);
         });*/
         /*jQuery(window).load(function(){
         setTimeout(function(){
         jQuery('a.asg-image-wrapper.asg-lightbox img').each(function() {
            var href = jQuery(this).parent().attr('href');
         console.log(href);
            jQuery(this).attr('src', href);
         });
         }, 1000);
         });*/
         jQuery(window).load(function(){
            jQuery('a.asg-image-wrapper.asg-lightbox img').each(function() {
               var href = jQuery(this).parent().attr('href');
         
               jQuery(this).attr('src', href);
            });
         
            setTimeout(openResize, 500);
            setTimeout(openResize, 1000);
            setTimeout(openResize, 1500);
            setTimeout(openResize, 2000);
            setTimeout(openResize, 2500);
            setTimeout(openResize, 3000);
         
            function openResize() {
               jQuery('a.asg-image-wrapper.asg-lightbox img').each(function() {
                  var src = jQuery(this).attr('src'),
                        src_new = src.replace('&zoom=1', '');
         
                  jQuery(this).attr('src', src_new);
               });
            }
         });
      </script>
   </body>
</html>