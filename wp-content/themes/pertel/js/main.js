jQuery(document).ready(function( $ ){ 

	
	// Initialize Slidebars
    var controller = new slidebars();
    controller.init();

    $(".menu-dropdown").on("click", function( event ){
        event.stopPropagation();
        event.preventDefault();
        controller.toggle( 'id-1' );
    })

    $( document ).on( 'click', function () {
        controller.close( function () {
          console.log( 'Closed the Slidebar.' );
        } );
    });

    //Slider
    var slider = $(".slider .box").unslider({

		nav : true,
		arrows: false,
		autoplay:true,
		delay:6000
	});

    $(".menu-info").on("click", function(){
    	$(this).next().slideToggle();
    })

    $('.full').slick({
    	arrows: true,
		asNavFor: '.thumb',
		slidesToShow: 1,
		fade: true,
    });
    $('.thumb').slick({
    	arrows: false,
    	vertical:true,
    	asNavFor: '.full',
    	slidesToShow: 4,
  		focusOnSelect: true
    });

    if( $("#map").length > 0 ){
  		initMap();
  	}

    if ( $(".featured-video-plus").length > 0 ) {
        $(".featured-video-plus").parent().addClass("remove-trans");
    };
    $(".various").fancybox({
      maxWidth  : 500,
      
      fitToView : false,
      width   : '70%',
      
      autoSize  : true,
      closeClick  : false,
      openEffect  : 'none',
      closeEffect : 'none',
      beforeShow : function(){
        if ( $(".servicio").length > 0) {
          $(".servicio").find("input").val($(this)[0].title);
        };
      }
    });
})

function initMap() {

  var park = {lat: -12.0725831, lng: -76.9911425};

  // Create a map object and specify the DOM element for display.
  var map = new google.maps.Map(document.getElementById('map'), {
    center: park,
    scrollwheel: false,
    zoom: 18,
    scrollwheel: false,
    draggable: false
  });


  var contentString = '<div id="content">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<h1 id="firstHeading" class="firstHeading">PERTEL</h1>'+
      '<div id="bodyContent">'+
      '<div> <p>Calle Mariscal Andrés de Santa Cruz 202/208 </p> <p>San Luis. Lima – Peru</p> <p> <a href="#">(01) 3267755</a> </p></div>'+
      '</div>'+
      '</div>';

  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });

  var marker = new google.maps.Marker({
    map: map,
    position: park,
    title: 'Hello World!',
    icon:site_url+'/wp-content/themes/pertel/img/marker-map.png',
  });

  marker.addListener('click', function() {
    infowindow.open(map, marker);
  });

  


}