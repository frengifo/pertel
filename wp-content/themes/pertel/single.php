<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
<section class="content-page blog">
        
    <section class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                	<?php if ( function_exists('yoast_breadcrumb') ) {
                            yoast_breadcrumb('
                            <p id="breadcrumbs" class="pull-right">','</p>
                            ');
                        }
                    ?>
                    <h2> <span><img src="<?php echo get_template_directory_uri() ?>/img/icon-blog.png"></span></span> Blog</h2>
                    
                </div>
            </div>
        </div>
    </section>
    <section class="contenido detalle">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-md-offset-9">
                    <form class="find-post" method="get" action="#">
                        <input type="search" name="search" placeholder="Busca una Noticia" />
                        <button type="submit"></button>
                    </form>
                </div>
                <div class="clear"></div>
                <div class="col-md-9 box">
                    <nav>
                    	<?php next_post_link( '%link', '<i class="fa fa-chevron-left" aria-hidden="true"></i> SIGUIENTE  ' ); ?>
                        <?php previous_post_link( '%link', 'ANTERIOR <i class="fa fa-chevron-right" aria-hidden="true"></i> ' ); ?>
                        
                    </nav>
                    <div class="clear"></div>
                    <article>
                        <figure>
                            <?php the_post_thumbnail("full"); ?>
                        </figure>
                        <h1>
                            <?php the_title(); ?>
                        </h1>
                        <div class="info">
                            <?php the_content(); ?>
                        </div>
                        <div class="tags">
                            <span><i class="fa fa-tags" aria-hidden="true"></i></span>
                            <?php 
                            	$tags = get_tags();
								$html = '<ul>';
								foreach ( $tags as $tag ) {
									$tag_link = get_tag_link( $tag->term_id );
											
									$html .= "<li>{$tag->name}, </li>";
								}
								$html .= '</ul>';
								echo $html;
                             ?>
                        </div>
                    </article>
                    <?php 

                    if ( comments_open() || get_comments_number() ) :
						comments_template( '/comments.php' );
					endif;

                     ?>
                </div>
                <?php $current_post_id = get_the_id(); ?>
                <?php get_template_part( 'content', 'aside-blog' ); ?>
                
            </div>
        </div>
    </section>
</section>
<?php endwhile; ?>
<?php get_footer(); ?>