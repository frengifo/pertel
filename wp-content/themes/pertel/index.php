<?php get_header(); ?>
<section class="content-page blog">
        
    <section class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php if ( function_exists('yoast_breadcrumb') ) {
                            yoast_breadcrumb('
                            <p id="breadcrumbs" class="pull-right">','</p>
                            ');
                        }
                    ?>
                    <h2> <span><img src="<?php echo get_template_directory_uri() ?>/img/icon-blog.png"></span></span> Blog</h2>
                    
                </div>
            </div>
        </div>
    </section>
    
    <section class="contenido">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-md-offset-9">
                    <form class="find-post" method="get" action="<?php echo site_url(); ?>/searchpage/">
                        <input type="text" name="search" placeholder="Busca una Noticia" />
                        <button type="submit"></button>
                    </form>
                </div>
                <div class="clear"></div>
                <section class="col-md-9 list-posts">
                    <div class="row">
                    <?php $i=1; ?>

                    <?php 
                        $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                        query_posts('post_type=post&post_status=publish&posts_per_page=6&paged='.$paged); ?>

                    <?php while ( have_posts() ) : the_post(); ?>
                        <article class="col-md-6">
                            <figure>
                                <a href="<?php the_permalink(); ?>" class="box-img">
                                    <?php the_post_thumbnail(); ?>
                                </a>
                                <figcaption>
                                    <h2>
                                        <a href="<?php the_permalink(); ?>">
                                            <?php the_title(); ?>
                                        </a>
                                    </h2>
                                    <div class="date">
                                        <span><?php the_date(); ?> </span> | por <?php the_author(); ?> 
                                    </div>
                                    <div class="excerpt">
                                        <?php echo excerpt(40); ?>
                                    </div>
                                    <a href="<?php the_permalink(); ?>" class="btn-vermas">LEER MÁS</a>
                                </figcaption>

                            </figure>
                        </article>
                        <?php if ($i % 2 == 0): ?>
                            <div class="clear"></div>
                        <?php endif ?>
                        <?php $i++; ?>
                    <?php endwhile; ?>
                    
                        
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?php 

                                the_posts_pagination( array(
                                    'mid_size'  => 2,
                                    'screen_reader_text' => ' ', 
                                    'prev_text' => __( '&#171; Anterior', 'pertel' ),
                                    'next_text' => __( 'Siguiente &#187;', 'pertel' ),
                                ) );

                             ?>
                        </div>
                    </div>
                </section>
                <?php wp_reset_query(); ?>

                <?php get_template_part( 'content', 'aside-blog' ); ?>
            </div>
        </div>
    </section>
</section>

<?php get_footer(); ?>