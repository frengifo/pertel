<?php 
/**
 * Template Name: Blog
 *
 */
?>
<?php get_header(); ?>
<section class="content-page blog">
        
    <section class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <?php if ( function_exists('yoast_breadcrumb') ) {
                            yoast_breadcrumb('
                            <p id="breadcrumbs" class="pull-right">','</p>
                            ');
                        }
                    ?>
                    <h2> <span></span> Blog</h2>
                    
                </div>
            </div>
        </div>
    </section>
    <section class="contenido">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-md-offset-9">
                    <form class="find-post" method="get" action="#">
                        <input type="search" name="search" placeholder="Busca una Noticia" />
                        <button type="submit"></button>
                    </form>
                </div>
                <div class="clear"></div>
                <section class="col-md-9 list-posts">
                    <div class="row">
                        <article class="col-md-6">
                            <figure>
                                <a href="#" class="box-img"><img src="img/noticia-blog.jpg" alt="titulo"></a>
                                <figcaption>
                                    <h2>
                                        <a href="">
                                            Expoenergía mantiene su clara apuesta por la eficiencia en...
                                        </a>
                                    </h2>
                                    <div class="date">
                                        <span>15 junio 2016 </span> | por Bibiana Gamboa
                                    </div>
                                    <div class="excerpt">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    </div>
                                    <a href="" class="btn-vermas">LEER MÁS</a>
                                </figcaption>

                            </figure>
                        </article>
                        <article class="col-md-6">
                            <figure>
                                <a href="#" class="box-img"><img src="img/noticia-blog.jpg" alt="titulo"></a>
                                <figcaption>
                                    <h2>
                                        <a href="">
                                            Expoenergía mantiene su clara apuesta por la eficiencia en...
                                        </a>
                                    </h2>
                                    <div class="date">
                                        <span>15 junio 2016 </span> | por Bibiana Gamboa
                                    </div>
                                    <div class="excerpt">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    </div>
                                    <a href="" class="btn-vermas">LEER MÁS</a>
                                </figcaption>

                            </figure>
                        </article>
                        <div class="clear"></div>
                        <article class="col-md-6">
                            <figure>
                                <a href="#" class="box-img"><img src="img/noticia-blog.jpg" alt="titulo"></a>
                                <figcaption>
                                    <h2>
                                        <a href="">
                                            Expoenergía mantiene su clara apuesta por la eficiencia en...
                                        </a>
                                    </h2>
                                    <div class="date">
                                        <span>15 junio 2016 </span> | por Bibiana Gamboa
                                    </div>
                                    <div class="excerpt">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    </div>
                                    <a href="" class="btn-vermas">LEER MÁS</a>
                                </figcaption>

                            </figure>
                        </article>
                        <article class="col-md-6">
                            <figure>
                                <a href="#" class="box-img"><img src="img/noticia-blog.jpg" alt="titulo"></a>
                                <figcaption>
                                    <h2>
                                        <a href="">
                                            Expoenergía mantiene su clara apuesta por la eficiencia en...
                                        </a>
                                    </h2>
                                    <div class="date">
                                        <span>15 junio 2016 </span> | por Bibiana Gamboa
                                    </div>
                                    <div class="excerpt">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    </div>
                                    <a href="" class="btn-vermas">LEER MÁS</a>
                                </figcaption>

                            </figure>
                        </article>
                    </div>
                </section>
                <aside class="col-md-3 recent-posts">
                    <div class="row">
                        <div class="col-md-12 heading">
                            <h2>RECIENTES</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-6 col-xs-12 box">
                        
                            <div class="row">
                                <figure class="col-md-5 col-sm-5 col-xs-5">
                                    <a href="">
                                        <img src="img/thumb-noticia.jpg">
                                    </a>
                                </figure>
                                <article class="col-md-7 col-sm-7 col-xs-7">
                                    <div class="date">
                                        15 de junio 2016
                                    </div>
                                    <a href="#">
                                        ¿Qué es la Eficiencia energética? Enterate de los tipos de energía...
                                    </a>
                                </article>
                            </div>

                        </div>
                        <div class="col-md-12 col-sm-6 col-xs-12 box">
                        
                            <div class="row">
                                <figure class="col-md-5 col-sm-5 col-xs-5">
                                    <a href="">
                                        <img src="img/thumb-noticia.jpg">
                                    </a>
                                </figure>
                                <article class="col-md-7 col-sm-7 col-xs-7">
                                    <div class="date">
                                        15 de junio 2016
                                    </div>
                                    <a href="#">
                                        ¿Qué es la Eficiencia energética? Enterate de los tipos de energía...
                                    </a>
                                </article>
                            </div>

                        </div>
                        <div class="col-md-12 col-sm-6 col-xs-12 box">
                        
                            <div class="row">
                                <figure class="col-md-5 col-sm-5 col-xs-5">
                                    <a href="">
                                        <img src="img/thumb-noticia.jpg">
                                    </a>
                                </figure>
                                <article class="col-md-7 col-sm-7 col-xs-7">
                                    <div class="date">
                                        15 de junio 2016
                                    </div>
                                    <a href="#">
                                        ¿Qué es la Eficiencia energética? Enterate de los tipos de energía...
                                    </a>
                                </article>
                            </div>

                        </div>
                        <div class="col-md-12 col-sm-6 col-xs-12 box">
                        
                            <div class="row">
                                <figure class="col-md-5 col-sm-5 col-xs-5">
                                    <a href="">
                                        <img src="img/thumb-noticia.jpg">
                                    </a>
                                </figure>
                                <article class="col-md-7 col-sm-7 col-xs-7">
                                    <div class="date">
                                        15 de junio 2016
                                    </div>
                                    <a href="#">
                                        ¿Qué es la Eficiencia energética? Enterate de los tipos de energía...
                                    </a>
                                </article>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 heading">
                            <h2>POPULARES</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-6 col-xs-12 box">
                        
                            <div class="row">
                                <figure class="col-md-5 col-sm-5 col-xs-5">
                                    <a href="">
                                        <img src="img/thumb-noticia.jpg">
                                    </a>
                                </figure>
                                <article class="col-md-7 col-sm-7 col-xs-7">
                                    <div class="date">
                                        15 de junio 2016
                                    </div>
                                    <a href="#">
                                        ¿Qué es la Eficiencia energética? Enterate de los tipos de energía...
                                    </a>
                                </article>
                            </div>

                        </div>
                        <div class="col-md-12 col-sm-6 col-xs-12 box">
                        
                            <div class="row">
                                <figure class="col-md-5 col-sm-5 col-xs-5">
                                    <a href="">
                                        <img src="img/thumb-noticia.jpg">
                                    </a>
                                </figure>
                                <article class="col-md-7 col-sm-7 col-xs-7">
                                    <div class="date">
                                        15 de junio 2016
                                    </div>
                                    <a href="#">
                                        ¿Qué es la Eficiencia energética? Enterate de los tipos de energía...
                                    </a>
                                </article>
                            </div>

                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </section>
</section>
<?php get_footer(); ?>