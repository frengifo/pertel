<?php get_header(); ?>



<section class="content-page productos">

        

    <section class="breadcrumbs">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <?php if ( function_exists('yoast_breadcrumb') ) {

                            yoast_breadcrumb('

                            <p id="breadcrumbs" class="pull-right">','</p>

                            ');

                        }

                    ?>

                     <h2> <span><img src="<?php echo get_template_directory_uri() ?>/img/icon-producto.png"></span> Productos</h2>

                    

                </div>

            </div>

        </div>

    </section>

    <section class="contenido">

        <div class="container big-box">

            <div class="row">

                <aside class="col-md-3 col-sm-12 col-xs-12">

                    <form class="find-prod" method="get" action="<?php echo site_url(); ?>/searchproductos/">

                        <input type="search" name="search" placeholder="Busca un Producto" />

                        <button type="submit"></button>

                    </form>

                    <a href="#" class="menu-info">CATEGORIAS <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span></a>

                    <nav>


                        <ul id="menu-menu-categoria-productos">
                            <?php wp_list_categories( array(
                                'taxonomy'            => 'categoria',
                                'hide_empty'          => 0,
                                'title_li'            => __( '' )
                            ) ); ?> 
                        </ul>
                        

                    </nav>


                </aside>

                <?php $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); ?>

                

                <?php 

					$args = array(

					  'name'        => 'productos',

					  'post_type'   => 'cabeceras',

					  'post_status' => 'publish',

					  'numberposts' => 1

					);

					$info_producto = get_posts($args);

				?>



                <div class="col-md-9 col-sm-12 col-xs-12 view">

                    <?php if ( $term ) { ?>

                    	<h2><?php echo  $term->name; ?></h2>

	                    <article>

	                        <p><?php echo  $term->description; ?></p>

	                    </article>

                    <?php }else{ ?>



                    	<h2><?php echo $info_producto[0]->post_title; ?></h2>

	                    <article>

	                        <p><?php echo $info_producto[0]->post_excerpt; ?></p>

	                    </article>



                    <?php } ?>

	                    

                    

                    <section class="list featured">

                        <div class="row">



                        	<?php  



	                        	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

								$args = array(

								  'posts_per_page' => 9,

								  'paged'          => $paged,

								  'post_type'   => 'productos',

                                  'orderby' => 'menu_order',

                                  'order'   => 'ASC'

								);



								$list = new WP_Query( $args ); 



                        	?>

                            <?php $i=1; ?>

                            <?php while ( $list->have_posts() ) : $list->the_post(); ?>

                                <div class="col-md-4 col-sm-6 box">

                                    <article>

                                        <figure>

                                            <a href="<?php the_permalink(); ?>">

                                                <img src="<?php the_post_thumbnail_url( 'full' ); ?>" alt="<?php the_title(); ?>" />

                                            </a>

                                        </figure>

                                        <h2>

                                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?> <br>

                                            <?php

                                                $terms = get_the_terms( get_the_id(), 'categoria' );

                                                $term = array_pop($terms);

                                            ?>

                                            <strong><?php echo $term->name ?></strong></a>

                                        </h2>

                                        

                                    </article>

                                </div>

                                <?php if ($i % 2 == 0): ?>

                                    <div class="clear show-modile"></div>

                                <?php endif ?>

                                <?php if ($i % 3 == 0): ?>

                                    <div class="clear hidden-mobile"></div>

                                <?php endif ?>

                                <?php $i++; ?>

                            <?php endwhile; ?>



                        </div>

                        <div class="row">

                        	<div class="col-md-12">

                        		<?php 



                        			the_posts_pagination( array(

										'mid_size'  => 2,

										'screen_reader_text' => ' ', 

										'prev_text' => __( '&#171; Anterior', 'pertel' ),

										'next_text' => __( 'Siguiente &#187;', 'pertel' ),

									) );



                        		 ?>

                        	</div>



                        </div>

                    </section>

                    

                </div>

            </div>

        </div>

    </section>

</section>



<?php get_footer(); ?>