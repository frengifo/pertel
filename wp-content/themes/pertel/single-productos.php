<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
<section class="content-page productos">
        
    <section class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php if ( function_exists('yoast_breadcrumb') ) {
                            yoast_breadcrumb('
                            <p id="breadcrumbs" class="pull-right">','</p>
                            ');
                        }
                    ?>
                    <h2> <span><img src="<?php echo get_template_directory_uri() ?>/img/icon-producto.png"></span> Productos</h2>
                    
                </div>
            </div>
        </div>
    </section>
    <section class="contenido">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <section class="title">
                        <h1><?php the_title(); ?></h1>
                        <h3><?php the_field("codigo"); ?></h3>
                    </section>
                </div>
                <div class="clear"></div>
                <section class="col-md-6 col-sm-6 col-xs-12 slick">
                    <div class="full">
                        <?php $fulls = $dynamic_featured_image->get_featured_images(get_the_id()) ?>
                        <?php foreach ($fulls as $key => $items): ?>
                            <div>
                                <img src="<?php echo $items['full'] ?>" alt="<?php the_title(); ?>">
                            </div>
                        <?php endforeach ?>
                        
                    </div>
                    <div class="thumb">
                        <?php foreach ($fulls as $key => $items): ?>
                            <div>
                                <img src="<?php echo $items['thumb'] ?>" alt="<?php the_title(); ?>">
                            </div>
                        <?php endforeach ?>
                        
                    </div>
                </section>

                <article class="col-md-6 col-sm-6 col-xs-12">
                    <div class="especificaciones">
                        <h2>ESPECIFICACIONES</h2>
                        <?php the_field('especificaciones') ?>
                    </div>
                    <div class="caracteristicas">
                        <h2>CARACTERISTICAS</h2>
                        <?php the_field('caracteristicas') ?>
                    </div>
                    <a href="#solicitar" class="solicitar various">SOLICITAR PRODUCTO</a>
                </article>
                <div class="clear"></div>
                <div class="col-md-4 aplicaciones">
                    <h2>APLICACIONES</h2>
                    <ul>
                        <?php $aplicaciones = wp_get_post_terms( get_the_id(), 'aplicaciones' ); ?>
                        <?php foreach ( $aplicaciones as $term ) { ?>
                            <li>
                                <img src="<?php the_field('imagen_aplicacion', $term); ?>" alt="<?php echo $term->name; ?>">
                                <small><?php echo $term->name; ?></small>
                            </li>    
                        <?php }?>
                            
                    </ul>
                </div>
                <div class="col-md-8 observaciones">
                    <h2>OBSERVACIONES ADICIONALES</h2>
                    <article>
                        <?php the_field('observaciones_adicionales') ?>
                    </article>
                </div>
                <div class="clear"></div>
                <div class="col-md-12 certificaciones">
                    <h2>CERTIFICACIONES</h2>
                    <ul>
                        <?php $certificaciones = wp_get_post_terms( get_the_id(), 'certificaciones' ); ?>
                        <?php foreach ( $certificaciones as $term ) { ?>
                            <li>
                                <img src="<?php the_field('certificacion', $term); ?>" alt="<?php echo $term->name; ?>">
                            </li>    
                        <?php }?>
                        
                    </ul>
                </div>

            </div>
        </div>
    </section>
    <section class="related featured">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="heading"> <strong> Productos </strong> relacionados </h2>
                </div>
                <div class="clear"></div>
                    
                <?php 

                $cats = get_the_terms( get_the_id(), 'categoria' );
                $cat = array_pop($cats);

                $featured_products = new WP_Query( array( 'post_type' => 'productos', 'posts_per_page' => '4', 'categoria' => $cat->slug,  'post__not_in' => array( get_the_id() ) ) ); ?>                
    
                <?php while ( $featured_products->have_posts() ) : $featured_products->the_post(); ?>
                    <div class="col-md-3 col-sm-6 box">
                        <article>
                            <figure>
                                <a href="<?php the_permalink(); ?>">
                                    <img src="<?php the_post_thumbnail_url( 'full' ); ?>" alt="the_title()" />
                                </a>
                            </figure>
                            <h2>
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?> <br>
                                <?php
                                    $terms = get_the_terms( get_the_id(), 'categoria' );
                                    $term = array_pop($terms);
                                ?>
                                <strong><?php echo $term->name ?></strong></a>
                            </h2>
                            
                        </article>
                    </div>
                <?php endwhile; ?>
                
            </div>
        </div>                                  
    </section>
</section>
<div class="hidden">
    <div id="solicitar">
        <?php echo do_shortcode( '[contact-form-7 id="197" title="Solicitar"]' ); ?>
    </div>
</div>
<?php endwhile; ?>
<?php get_footer(); ?>
