<?php 
/**
 * Template Name: Clientes
 *
 */
?>
<?php get_header(); ?>
<section class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <?php if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb('
                        <p id="breadcrumbs" class="pull-right">','</p>
                        ');
                    }
                ?>
                <h2> <span><img src="<?php echo get_template_directory_uri() ?>/img/icon-nosotros.png"></span> <?php the_title(); ?></h2>
                
            </div>
        </div>
    </div>
</section>
<div class="container clientes services" style="padding: 4em 0 4em;">

	<div class="row">
		<div class="col-md-12 heading" style="margin-bottom: 2em;">
            <h2>Principales Clientes</h2>
        </div>
	<?php $cli = new WP_Query( array(  'post_type' => 'clientes' ) ); ?>
	<?php while ( $cli->have_posts() ) : $cli->the_post(); ?>
		<div class="col-md-4">
			<article style="margin:1em;"><?php the_post_thumbnail("full"); ?></article>
		</div>
	<?php endwhile; ?>
	</div>
</div>

<?php get_footer(); ?>