<?php 

	

	add_theme_support( 'post-thumbnails', array( 'productos', 'servicios', 'post', 'page', 'clientes' ) );



	function register_my_menu() {

	  register_nav_menu('header-menu',__( 'Header Menu' ));

	}

	add_action( 'init', 'register_my_menu' );



	function wpse_81939_post_types_admin_order( $wp_query ) {

	  if (is_admin()) {



	    // Get the post type from the query

	    $post_type = $wp_query->query['post_type'];



	    if ( $post_type == 'productos') {



	      $wp_query->set('orderby', 'date');



	      $wp_query->set('order', 'DESC');

	    }

	  }

	}

	add_filter('pre_get_posts', 'wpse_81939_post_types_admin_order');

	

	// get the the role object

	$role_object = get_role( 'editor' );



	// add $cap capability to this role object

	$role_object->add_cap( 'edit_theme_options' );



	add_action('pre_get_posts','set_study_parent');



	function set_study_parent( $query ) {

	  if ( ! is_admin() && is_main_query() && is_tax('categoria') ) {

	    $query->set( 'post_parent', 0 );

	  }

	}



	/*CODIGO PARA FIXEAR PAGINACION EN TAXONOMIA*/

	function fix_taxonomy_pagination ( $query ) {

	  // not an admin page and it is the main query

	  if (!is_admin() && $query->is_main_query()){



	    if(is_tax()){

	      // where 24 is number of posts per page on custom taxonomy pages

	      	$query->set('posts_per_page', 9);



	    }

	    //Setear paginacion productos max 9 por pagina

	    if($query->query['post_type'] == "productos"){

	    	$query->set('posts_per_page', 9);

	    }

	  }

	}

	add_action( 'pre_get_posts', 'fix_taxonomy_pagination' );



function excerpt($limit) {

  $excerpt = explode(' ', get_the_excerpt(), $limit);

  if (count($excerpt)>=$limit) {

    array_pop($excerpt);

    $excerpt = implode(" ",$excerpt).'...';

  } else {

    $excerpt = implode(" ",$excerpt);

  }	

  $excerpt = preg_replace('`[[^]]*]`','',$excerpt);

  return $excerpt;

}





function my_custom_popular_posts_html_list( $mostpopular, $instance ){

    //$output = '<ol class="wpp-list">';



    // loop the array of popular posts objects

    $i=0;

    foreach( $mostpopular as $popular ) {



    	//var_dump($popular);



        $stats = array(); // placeholder for the stats tag



        // Comment count option active, display comments

        if ( $instance['stats_tag']['comment_count'] ) {

            // display text in singular or plural, according to comments count

            $stats[] = '<span class="wpp-comments">' . sprintf(

                _n('1 comment', '%s comments', $popular->comment_count, 'wordpress-popular-posts'),

                number_format_i18n($popular->comment_count)

            ) . '</span>';

        }



        if ( get_post_type( $popular->id ) == 'post' ) {



			$out .=  '<div class="col-md-12 col-sm-6 col-xs-12 box">' ;

			$out .=      '<div class="row">' ;

			$out .=                    '<figure class="col-md-5 col-sm-5 col-xs-5">' ;

			$out .=                        '<a href="'.get_the_permalink( $popular->id ).'">' ;

			$out .=                            get_the_post_thumbnail($popular->id, 'thumbnail') ;

			$out .=                        '</a>' ;

			$out .=                    '</figure>' ;

			$out .=             '<article class="col-md-7 col-sm-7 col-xs-7">' ;

			$out .=                  '<div class="date">'.get_the_date( "j F, Y", $popular->id ).'</div>' ;

			$out .=                  '<a href="'.get_the_permalink( $popular->id ).'">'. esc_attr( $popular->title ).'</a>' ;

			$out .=             '</article>' ;

			$out .=        '</div>' ;

			$out .=  '</div>';



			$i++;



        }



        if ( $i == 3 ) {

        	break;

        }



    }



    



    return $out;

}



add_filter( 'wpp_custom_html', 'my_custom_popular_posts_html_list', 10, 2 );





function mytheme_comment($comment, $args, $depth) { ?>

    

    <div class="comment-author vcard row">

        <div class="col-md-12 box">

        	<div class="avatar col-md-2 col-sm-2">

	        	<?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?>

	        </div>

	        <div class="content col-md-10 col-sm-10">

	        	<div class="reply">

			        <?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>

			    </div>

	        	<h4><?php comment_author(); ?></h4>

	        	<div class="comment-date"><?php

			        /* translators: 1: date, 2: time */

			        printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Edit)' ), '  ', '' );

		        ?></div>

	        	<article><?php comment_text(); ?></article>

	        </div>

        </div>

    </div>

    <div class="clear"></div>

    <?php if ( $comment->comment_approved == '0' ) : ?>

         <em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ); ?></em>

          <br />

    <?php endif; 



    }





/**

 * Enqueue scripts and styles.

 *

 * @since Twenty Fifteen 1.0

 */

function pertel_scripts() {

	

	

    



    if ( is_singular( 'productos' ) || is_tax( 'tipo' ) ) {

		// Add Genericons, used in the main stylesheet.

		wp_enqueue_style( 'fancybox-style', get_template_directory_uri() . '/js/vendor/fancybox/source/jquery.fancybox.css', array(), '2.1.5' );

		wp_enqueue_script( 'fancybox-script', get_template_directory_uri() . '/js/vendor/fancybox/source/jquery.fancybox.pack.js', array(), '2.1.5');

		// Get the post type from the query

	}

}

add_action( 'wp_enqueue_scripts', 'pertel_scripts' );



function the_slug($echo=true){

  $slug = basename(get_permalink());

  do_action('before_slug', $slug);

  $slug = apply_filters('slug_filter', $slug);

  if( $echo ) echo $slug;

  do_action('after_slug', $slug);

  return $slug;

}





function search_excerpt_highlight() {

    $excerpt = get_the_excerpt();

    $keys = implode('|', explode(' ', 'ups'));

    $excerpt = preg_replace('/(' . $keys .')/iu', '<strong class="search-highlight">\0</strong>', $excerpt);



    echo '<p>' . $excerpt . '</p>';

}



function search_title_highlight() {

    $title = get_the_title();

    $keys = implode('|', explode(' ', get_search_query()));

    $title = preg_replace('/(' . $keys .')/iu', '<strong class="search-highlight">\0</strong>', $title);



    echo $title;

}




/*-------------------------------------------------------------------------------
	Custom Columns
-------------------------------------------------------------------------------*/

add_filter( 'manage_productos_posts_columns', 'set_custom_edit_productos_columns' );
add_action( 'manage_productos_posts_custom_column' , 'custom_productos_column', 10, 2 );

function set_custom_edit_productos_columns($columns) {
    unset( $columns['author'] );
    $columns['productos_codigo'] = __( 'Codigo', 'pertel' );
    $columns['imagen'] = __( 'Imagen', 'pertel' );

    return $columns;
}

function custom_productos_column( $column, $post_id ) {
    switch ( $column ) {

        case 'productos_codigo' :
            echo get_field( 'codigo', $post->ID);
            break;
        case 'imagen' :
            echo wp_get_attachment_image( get_post_thumbnail_id( $post->ID ), array(50,50) );
            break;

    }
}


 ?>