<?php 
	
	$args = array(
	'p' => 241, // id of a page, post, or custom type
	'post_type' => 'any');
	$footer = new WP_Query($args);

 ?>
 	<?php while ( $footer->have_posts() ) : $footer->the_post(); ?>
			<footer>
                <div class="container">
                	<div class="row">
                		<div class="col-md-8 col-sm-12 col-xs-12">
                			<div class="row">
                				<div class="col-md-7 col-sm-6">
                					<a href="#"> 
		                				<img src="<?php echo get_template_directory_uri() ?>/img/logo-pertel.png" alt="Logo Pertel" />
		                			</a>
		                			<?php the_field("texto"); ?>
                				</div>
                				<div class="col-md-5 col-sm-6">
                					<h2>CONTÁCTANOS</h2>
                					<ul>
                						<li class="tel">
                							<p>
                								<?php the_field('telefonos'); ?>
                							</p>
                						</li>
                						<li class="dir">
                							<p><?php the_field('direccion'); ?></p>
                						</li>
                                        <li class="email">
                                            <p><?php the_field('email'); ?></p>
                                        </li>
                					</ul>
                					<div class="redes">
                						<a href="<?php the_field('facebook'); ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                						<a href="<?php the_field('twitter'); ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                					</div>
                				</div>
                			</div>
                		</div>
                		<div class="col-md-4 col-sm-12 col-xs-12 suscribe">
                			<h2>SUSCRÍBETE A NUESTRO BOLETÍN</h2>
                			<?php echo do_shortcode( '[contact-form-7 id="198" title="Suscríbete"]' ); ?>
                		</div>
                	</div>
                </div>
                <div class="copy">
                	<p>Copyright 2016 <a href="#">Pertel</a> | Diseñado y Desarrollado por <a href="#">Reder Design</a></p>
                </div>				
            </footer>
    <?php endwhile; ?>
        </div>
        <?php wp_footer(); ?>
        
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/vendor/slidebars.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/plugins.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/main.js?v=3"></script>
        <script type="text/javascript">var site_url = "<?php echo site_url(); ?>";</script>
    </body>
</html>