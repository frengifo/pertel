<div id="comments" class="comments-area row">

	<div class="col-md-12">
		
		<?php if ( have_comments() ) : ?>
			<a href="#commentform" class="pull-right">
				<i class="fa fa-pencil" aria-hidden="true"></i> &nbsp; &nbsp; Deja un Comentaio
			</a>
			<h2 class="comments-title">
				<?php echo get_comments_number(); ?> Comentarios
			</h2>

			

			<ol class="comment-list row">
				<?php
					wp_list_comments( 'type=comment&callback=mytheme_comment' );
				?>
			</ol><!-- .comment-list -->
					
			
		<?php endif; // have_comments() ?>

		<?php
			// If comments are closed and there are comments, let's leave a little note, shall we?
			if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
		?>
			<p class="no-comments"><?php _e( 'Comments are closed.', 'pertel' ); ?></p>
		<?php endif; ?>

		<?php 

			$comments_args = array(
		        // change the title of send button 
		        'label_submit'=>'DEJA UN COMENTARIO',
		        // change the title of the reply section
		        'title_reply'=>'Déjanos un comentario',
		        // remove "Text or HTML to be displayed after the set of comment fields"
		        'comment_notes_before' => '',
		        // redefine your own textarea (the comment body)
		        'comment_field' => '<textarea id="comment" name="comment" aria-required="true"></textarea></p>',
			);

			comment_form($comments_args);


		 ?>

		

	</div>

</div><!-- .comments-area -->
