<aside class="col-md-3 recent-posts">
                    
                    <div class="row">
                        <div class="col-md-12 heading">
                            <h2>POPULARES</h2>
                        </div>
                    </div>
                    <div class="row">
                        <?php wpp_get_mostpopular(); ?>
                    </div>
                    <div class="row">
                        <div class="col-md-12 heading">
                            <h2>RECIENTES</h2>
                        </div>
                    </div>
                    <div class="row">
                <?php if (is_single()){
                    $recent_posts = new WP_Query( array(  'post_type' => 'post', 'posts_per_page' => 3, 'post__not_in' => array( get_the_id() ) ) );
                }else{ 
                    $recent_posts = new WP_Query( array(  'post_type' => 'post', 'posts_per_page' => 3 )  );
                } ?>
                    

                    <?php while ( $recent_posts->have_posts() ) : $recent_posts->the_post(); ?>
                            <div class="col-md-12 col-sm-6 col-xs-12 box">
                        
                                <div class="row">
                                    <figure class="col-md-5 col-sm-5 col-xs-5">
                                        <a href="<?php the_permalink(); ?>">
                                            <?php the_post_thumbnail( 'thumbnail' ) ; ?>
                                        </a>
                                    </figure>
                                    <article class="col-md-7 col-sm-7 col-xs-7">
                                        <div class="date">
                                            <?php echo get_the_date( 'j F, Y' ); ?>
                                        </div>
                                        <a href="<?php the_permalink(); ?>">
                                            <?php the_title(); ?>
                                        </a>
                                    </article>
                                </div>

                            </div>
                    <?php endwhile; ?>

                        
                    </div>
                    

                    <div class="row">
                        <div class="col-md-12 heading">
                            <div class="fb-page" data-href="https://www.facebook.com/PERTELDEPERU/?fref=ts" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>
                        </div>
                    </div>
                </aside>