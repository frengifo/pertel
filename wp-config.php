<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'pertelde_web');

/** MySQL database username */
define('DB_USER', 'pertelde_web');

/** MySQL database password */
define('DB_PASSWORD', 'admin@2016');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Z7mHz*oRIq6+TNfe[v{MZG;Vgj#[|dR02@_L&=B4dw~uAn1eSkPq[.n{hW>5-(L7');
define('SECURE_AUTH_KEY',  'S*Ef{sz6{m3g+q-UUz6>W8;KgKf3ebp93;ps ~ZvRPu/n%^}NA&KG1@hyLOW`6&.');
define('LOGGED_IN_KEY',    'aWK+[C%KQ/IAmH1`,jzq15~A.7JX@y``CX6&rD)B%qu:Y(iYTX@_~y>[cJ!>OTo7');
define('NONCE_KEY',        '_W+ZFknZ-X>u7mFfB >:,r<^jEz7!>)FdHv|_S>CKHA=K0Gz?GzR-1T9~ua|D*f1');
define('AUTH_SALT',        '~?ru[PZ[QHPB{dfg53=@|}0xY7nyrIs`]k10zi1WN~0i~lbUZaze;=mDnb~d!k~6');
define('SECURE_AUTH_SALT', 'i<<gho}q1_H8#fkf.R&Vb2Jxk,/B1:Ko$Fv;,Iy>M?.|?r1=i PER3;UN`|l5N$s');
define('LOGGED_IN_SALT',   'Xilk#t`*%oSDf:Mr57hvl?GYUCHSHR2(q| {|Asc`WoT]-z`5`B-KR!TGedi:V-I');
define('NONCE_SALT',       'A1bwiHHz ,Bh1hXJsTrHGozT%HE.U7N<@(F)Z9vS<jZCZ*f<PJm^gIN]Ox]Qp%*M');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wptel_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
